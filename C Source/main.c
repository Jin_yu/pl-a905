/*************************************************************
  20190424 main system from PL305(90418) to PL905 system 

***************************************************************/
#include <MPC82G516.h>
#include <intrins.h>
#include <LCM.h>
#include <LED.h>
#include <I2C.h>
#include <MCP4011.h>
#include <COUNTER.H>
#include <LANGUAGE.H>
#include <ADC.H>
#include <STDLIB.H>

#define MAX_LITER			9000
#define MAX_HOUR			8760
#define VERSION_Y			9
#define VERSION_MD		424

#define STANDBY_MODE						0
#define	ELECTROLYSIS_MODE			  1 
#define	SLEEP_MODE							2
#define	SETUP_MODE							3
#define	CLEAN_MODE							4
#define	WARNING_MODE						5
#define	RESET_MODE							6
#define	CLEAN_W_ELE_MODE 			 	7
#define	CHANGE_FILTER_MODE		 	8
#define START_UP_MODE 				  9
#define CLEAN_WAITING_MODE 		 	10
#define START_UP_CLEAN_MODE 	 	11
#define	WARNING_BOARD_HOT_MODE	12
#define	WARNING_POT_FAIL_MODE		13


#define LAN_CHINESE 0
#define LAN_ENGLISH 2
#define LAN_CZECH 3
#define LAN_ITALY 4
#define LAN_FRENCH 5

#define	Area_Eng		1
#define	Area_Cht_N	3
#define	Area_Cht_M	4
#define	Area_Cht_S	5
#define	Area_Cht_E	6


#define SET_TDS 0
#define SET_SEGMENT 1
#define SET_CURRENT 2
#define SET_LANGUAGE 3
#define SET_WASH 4


#define	WALKA4		1
#define	WALKA3		2
#define	WALKA2		3
#define	WALKA1		4
#define	WFILTER		5
#define	WACID1		6
#define	WACID2		7
#define	WACID3		8

#define DEFAULT_WASHLITER 30 

#define Over_ALK4 0xA001
#define Over_ALK3 0xA002
#define Over_ALK2 0xA004
#define Over_ALK1 0xA008
#define Over_FILTER 0xA010
#define Over_ACI1 0xA020
#define Over_ACI2 0xA040
#define Over_ACI3 0xA080
#define Over_ELE_T 0xA100
#define Over_OUT_C 0xA200

#define ADC1115_VE_6P9V 0x2328
#define ADC1115_OVER_HOT 0x3D00//170 degree

#define PJ6000_1_CLEAN_TIME 30
#define PJ6000_2_CLEAN_TIME 1800          //Orignal Why 7200?
#define PJ6000_3_CLEAN_TIME 300
#define PJ6000_TOTAL_CLEAN_TIME (PJ6000_1_CLEAN_TIME+PJ6000_2_CLEAN_TIME+PJ6000_3_CLEAN_TIME)

#define F_CLEAN 0
#define F_NO_CLEAN 1
extern unsigned int KX;	
extern unsigned int R;	
extern unsigned char key_delay[16];

sbit LCM_DataLE	    = P2^0; 
sbit LED_DataLE	    = P2^1;
sbit IO_DataLE		= P2^2;
sbit IO_DataOE		= P3^5;
sbit V_DETECT		= P1^0;
sbit HOT_CHK        = P1^1;
sbit Choose_Default = P4^1;
sbit INT            = P3^3;      //Trasfer to SAM3S


bit f_half_sec, f_key_hold, f_no_water, f_alarm_no_water;
bit f_acid3=0, f_clean=0, f_lock=0, f_show_orp=0;
unsigned char Operation_Mode,ADS1115_Check_flag=0,ex_op_mode,clean_after_trun_off=0,TPL0401A_ACK=0,tpl0401a_value=0,ex_tpl0401a_value=0,ADS1115_ACK,start_up_mode=0;
unsigned char electrolysis_type;
idata unsigned char T0_cnt, T1_cnt;
unsigned int FLOW_cnt = 0,ex_ads1115_value=0,ads1115_temp=0;
unsigned char tmp1,POT_Fail_Flag=0,Faucent_Version_Flag=0,R03_R05N=0;
unsigned char VP02 = 0;
unsigned char ADH;
unsigned char ADL;
idata unsigned char Soft_Timer[8]=0;
unsigned char Voice_Timer=0; //20190326 jin
unsigned char Voice_timer_flag=0; //20190326 jin
unsigned char Voice_Timer_2=0; //20190411 jin
unsigned char Voice_timer_flag_2=0; //20190411 jin
unsigned int Soft_Timer_PJ6000[2]={0};
unsigned char Soft_Timer_Enable = 0;
unsigned char SleepEnable, Key_backup;
unsigned char PVoice = 0;
unsigned char PVoice_tmp = 0;//20190411
unsigned char Volume;
unsigned char Language,  r_step, r_step_max, r_step_cnt;
unsigned char ReceiveDate,ReceiveFlag;
unsigned char Control_Selection;
unsigned char ErrCode, aci_flag=0, after_action=0,ex_I=0,ex_alk_flag=0,ex_electrolysis_type=0;
int kk, I_add_t, I_add[8];
unsigned int Liter, WashLiter, left_FlowCnt, UseMin, UseHour, LeftLiter, max_hour_count=MAX_HOUR;
unsigned int LeftLiter, flow_cnt_tmp[2]={0,0};

//unsigned char ele_set[7]={10,5,4,2,7,10,20};
//unsigned char ele_set[7]={10,4,3,1,4,10,20};
unsigned char ele_set[7]={22,5,4,2,5,12,50};//20190213 jin

	
unsigned char demo_flag = 0,ADS1110_flag=0;
unsigned char	do_clean_flag=0; //20190319 jin
unsigned int overflow_flag,Over_each_flag[10]={Over_ALK4,Over_ALK3,Over_ALK2,Over_ALK1,Over_FILTER,Over_ACI1,Over_ACI2,Over_ACI3,Over_ELE_T,Over_OUT_C};//xxxxxxxx xxxxxxxx
/* Set overflow flag when the record variable of unsigned long int type is greater than or equals to 4294967295
overflow_flag: 0 Alka4, 1 Alka3, 2 Alka2, 3 Alka1
							 4 Filter, 5 Acid1, 6 Acid2, 7 Acid3
							 8 electrolyze_time
							 9 Outwater_Counter

*/
unsigned long int Usage_Counter[8],electrolyze_time,Outwater_Counter;



void uart_init(void)	// Set UART Baud Rate to 115200 bps
{
	 SCON |= 0x50;					  /* uart in mode 1 (8 bit), REN=1 */
   T2CON &= 0xF0;               /* EXEN2=0; TR2=0; C/T2#=0; CP/RL2#=0; */
   T2CON |= 0x30;               /* RCLK = 1; TCLK=1; */
   T2MOD &= 0xFC;								/* T2OE =0; DCEN = 0;*/	
   //TH2=0xFF;                    /* init value */
   //TL2=0xFD;                    /* init value */
   //RCAP2H=0xFF;                 /* reload value, 115200 Bds at 11.059MHz */
   //RCAP2L=0xFD;                 /* reload value, 115200 Bds at 11.059MHz */
   TH2=0xFF;                    /* init value */
   TL2=0xD8;                    /* init value */
   RCAP2H=0xFF;                 /* reload value, 9600 Bds at 11.059MHz */
   RCAP2L=0xD8;                 /* reload value, 9600 Bds at 11.059MHz */
	 ES = 1; 						     /* Enable serial interrupt */
   TR2 = 1;                     /* Timer 2 run */
}

void serial() interrupt 4
{

	if(TI==1)
	{
	   TI=0;
	}
	if(RI==1)
	{
	   RI=0;
	   ReceiveDate=SBUF;
		 ReceiveFlag = 1;
		 if(ReceiveDate==__KEY_A705R04__)
		 {
				ReceiveDate = 0;
				Faucent_Version_Flag=1;
		 }
	}
}
void sendchar(unsigned char SendData)
{
		SBUF=SendData;
		while(TI==0);
}
void SendVersion(void)
{
	/*
	***************    <<<<<< >>>>>>     ********************
			UART DATA: 0x_ _
			0x0 _ : version_date[0]
			0x1 _ : version_date[1]
			0x2 _ : version_date[2]
			0x3 _ : version_date[3]
			0x4 _ : version_year

	=================================
	*/
		unsigned char version_date[4]={0};
		static unsigned char i =0;
				version_date[0] = (VERSION_MD/1000)+0x00;
				version_date[1] = ((VERSION_MD%1000)/100)+0x10;
				version_date[2] = ((VERSION_MD%100)/10)+0x20;
				version_date[3] = (VERSION_MD%10)+0x30;

				if(i==0)
				{
					sendchar(((VERSION_Y&0x0F)+0x40));
					sendchar(version_date[0]);
					i++;
				}	
				else if(i==1)
				{
					sendchar(version_date[1]);
					sendchar(version_date[2]);
					i++;	
				}
				else
				{
					sendchar(version_date[3]);

					i=0;
				}


}
void SendPJ_6000_Time(void)
{
	/*
	***************    <<<<<< >>>>>>     ********************
			UART DATA: 0x_ _
			0x6 _ : PJ6000_TOTAL_CLEAN_TIME[0]
			0x7 _ : PJ6000_TOTAL_CLEAN_TIME[1]
			0x8 _ : PJ6000_TOTAL_CLEAN_TIME[2]
			0x9 _ : PJ6000_TOTAL_CLEAN_TIME[3]
			0xA _ : PJ6000_TOTAL_CLEAN_TIME[4]

	=================================
	*/
		unsigned char pj6000_time[5]={0};
		unsigned char i;
		pj6000_time[4] = (PJ6000_TOTAL_CLEAN_TIME/10000)+0x60;
		pj6000_time[3] = ((PJ6000_TOTAL_CLEAN_TIME%10000)/1000)+0x70;
		pj6000_time[2] = ((PJ6000_TOTAL_CLEAN_TIME%1000)/100)+0x80;
		pj6000_time[1] = ((PJ6000_TOTAL_CLEAN_TIME%100)/10)+0x90;
		pj6000_time[0] = (PJ6000_TOTAL_CLEAN_TIME%10)+0xA0;
		
		for(i=0;i<8;i++)
		{
			sendchar(pj6000_time[0]);
			sendchar(pj6000_time[1]);
			sendchar(pj6000_time[2]);
			sendchar(pj6000_time[3]);
			sendchar(pj6000_time[4]);
		}
}

void SendHideInfo(unsigned char sel_data)
{
	/*
	***************    <<<<<< >>>>>>     ********************
			UART long int - DATA:  0x 7 6 5 4 3 2 1 0
				0x0 * 							
				0x1 _ 							
				0x2 _ 							
				0x3 _ 							
				0x4 _ 							
				0x5 _ 							
				0x6 _ 							
				0x7 _ 	
			UART int - DATA:overflow_flag 0x8 9 A B
				0x8 _
				0x9 _
				0xA _
				0xB _
	=================================
	*/
		unsigned char hide_date[12]={0};
		hide_date[8]=(overflow_flag&0x000F)+0x80;
		hide_date[9]=((overflow_flag&0x00F0)>>4)+0x90;
		hide_date[10]=((overflow_flag&0x0F00)>>8)+0xA0;
		hide_date[11]=((overflow_flag&0xF000)>>12)+0xB0;
		switch(sel_data)
		{
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
				case 5:
				case 6:
				case 7:
					hide_date[0]=(Usage_Counter[sel_data]&0x0000000F)+0x00;
					hide_date[1]=((Usage_Counter[sel_data]&0x000000F0)>>4)+0x10;
					hide_date[2]=((Usage_Counter[sel_data]&0x00000F00)>>8)+0x20;
					hide_date[3]=((Usage_Counter[sel_data]&0x0000F000)>>12)+0x30;
					hide_date[4]=((Usage_Counter[sel_data]&0x000F0000)>>16)+0x40;
					hide_date[5]=((Usage_Counter[sel_data]&0x00F00000)>>20)+0x50;
					hide_date[6]=((Usage_Counter[sel_data]&0x0F000000)>>24)+0x60;
					hide_date[7]=((Usage_Counter[sel_data]&0xF0000000)>>28)+0x70;	
					break;
				case 8:
					hide_date[0]=(electrolyze_time&0x0000000F)+0x00;
					hide_date[1]=((electrolyze_time&0x000000F0)>>4)+0x10;
					hide_date[2]=((electrolyze_time&0x00000F00)>>8)+0x20;
					hide_date[3]=((electrolyze_time&0x0000F000)>>12)+0x30;
					hide_date[4]=((electrolyze_time&0x000F0000)>>16)+0x40;
					hide_date[5]=((electrolyze_time&0x00F00000)>>20)+0x50;
					hide_date[6]=((electrolyze_time&0x0F000000)>>24)+0x60;
					hide_date[7]=((electrolyze_time&0xF0000000)>>28)+0x70;	
					break;
				case 9:
					hide_date[0]=(Outwater_Counter&0x0000000F)+0x00;
					hide_date[1]=((Outwater_Counter&0x000000F0)>>4)+0x10;
					hide_date[2]=((Outwater_Counter&0x00000F00)>>8)+0x20;
					hide_date[3]=((Outwater_Counter&0x0000F000)>>12)+0x30;
					hide_date[4]=((Outwater_Counter&0x000F0000)>>16)+0x40;
					hide_date[5]=((Outwater_Counter&0x00F00000)>>20)+0x50;
					hide_date[6]=((Outwater_Counter&0x0F000000)>>24)+0x60;
					hide_date[7]=((Outwater_Counter&0xF0000000)>>28)+0x70;	
					break;		
			}				
			sendchar(hide_date[0]);
			sendchar(hide_date[1]);
			sendchar(hide_date[2]);
			sendchar(hide_date[3]);
			sendchar(hide_date[4]);
			sendchar(hide_date[5]);
			sendchar(hide_date[6]);
			sendchar(hide_date[7]);		
			sendchar(hide_date[8]);
			sendchar(hide_date[9]);
			sendchar(hide_date[10]);
			sendchar(hide_date[11]);
			
}

void SendMode(unsigned char mode_sel, unsigned int uart_data)
{
	/*
		sbit GPIO = P4^1;//control by SAM3S and transfer to SAM3S
		sbit INT = P3^3;//Trasfer to SAM3S
		Transfer 4 bits deata to SAM3S by UART
	***************    <<<<<< >>>>>>     ********************
			UART DATA: 0x_ _
			0x0 _ : Mode( include Error mode)
			0x1 _ : F0(L)
			0x2 _ : F0(H)
			0x3 _ : LeftLiter (0)
			0x4 _ : LeftLiter (1)
			0x5 _ : LeftLiter (2)
			0x6 _ : LeftLiter (3)
			0x7 _ : UseHour  (0)
			0x8 _ : UseHour (1)
			0x9 _ : UseHour (2)
			0xA _ : UseHour (3)	
			0xB _ : Area
			0xC _ : Confirm to ARAM for connection
			0xD _ : ACID3 Flag
			0xE _ : WARING Mode 0: ER1, 3:ER3, 4:ER4
	Mode:
			#define 	STANDBY_MODE		0
			#define	ELECTROLYSIS_MODE	1 
			#define	SLEEP_MODE		2
			#define	SETUP_MODE		3
			#define	CLEAN_MODE		4 <-  7 with elector ->
			#define	WARNING_MODE		5
			#define	RESET_MODE		6
			#define	CHANGE_FILTER_MODE 8
			#define START_UP_MODE 9
			#define CLEAN_WAITING_MODE 10
			
			
	=================================
	*/
		unsigned char flow[2]={0},op_mode,area;
		static unsigned char i =0;
		unsigned int left_liter[4]={0},use_hour[4]={0};
		switch(mode_sel)
		{
			case 0:						
				op_mode = uart_data;
				if(op_mode == WARNING_MODE)
					if(ErrCode==4)
						sendchar(0x00E4);
					else if(ErrCode==3)
						sendchar(0x00E3);
					else
						sendchar(0x00E1);
				if(f_clean)	
				{
					if(Faucent_Version_Flag)
					{
						if(start_up_mode) 
							sendchar(START_UP_CLEAN_MODE);						
						else 
						//	sendchar(START_UP_CLEAN_MODE);	
							sendchar(CLEAN_WAITING_MODE);//Ken	121227 clean wait mode
					}
					else
						sendchar(CLEAN_W_ELE_MODE);//clean mode with elector
				}
				else if (((LeftLiter < 900)||(UseHour > 7884)) && (op_mode != WARNING_MODE)) sendchar(CHANGE_FILTER_MODE);
				else sendchar(0x00 + op_mode);	
				if(f_acid3)
							sendchar(0x00D1);
				else
							sendchar(0x00D0);
				break;
			case 1:
				flow[0] = (uart_data&0x000F)+0x10;
				flow[1] = ((uart_data&0x00F0)>>4) + 0x20;//(uart_data&0x00F0)>>4+0x20;
				sendchar(flow[0]);_nop_();_nop_();_nop_();_nop_();_nop_();
				sendchar(flow[1]);_nop_();_nop_();_nop_();_nop_();_nop_();
				break;
			case 2:
				left_liter[0] = (LeftLiter&0x000F)+0x0030;
				left_liter[1] = ((LeftLiter&0x00F0)>>4)+0x0040;
				left_liter[2] = ((LeftLiter&0x0F00)>>8)+0x0050;
				left_liter[3] = ((LeftLiter&0xF000)>>12)+0x0060;

			
			  
				use_hour[0] = (UseHour&0x000F)+0x0070;
				use_hour[1] = ((UseHour&0x00F0)>>4)+0x0080;
				use_hour[2] = ((UseHour&0x0F00)>>8)+0x0090;
				use_hour[3] = ((UseHour&0xF000)>>12)+0x00A0;
			
				/*	
				use_hour[0] = (UseMin&0x000F)+0x0070;
				use_hour[1] = ((UseMin&0x00F0)>>4)+0x0080;
				use_hour[2] = ((UseMin&0x0F00)>>8)+0x0090;
				use_hour[3] = ((UseMin&0xF000)>>12)+0x00A0;*/			
				if(i==0)
				{
					sendchar(left_liter[2]);
					sendchar(left_liter[3]);
					i++;
				}	
				else if(i==1)
				{
					sendchar(left_liter[0]);
					sendchar(left_liter[1]);
					i++;	
				}
				else if(i==2)
				{
					sendchar(use_hour[0]);
					sendchar(use_hour[1]);
					i++;
				}
				else if(i==3)
				{
					sendchar(use_hour[2]);
					sendchar(use_hour[3]);		
					i++;	
				}				
				else
				{
						if(f_clean)	sendchar(CLEAN_W_ELE_MODE);
						else if(f_acid3)
							sendchar(0x00D1);
						else
							sendchar(0x00D0);
						_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();	
						sendchar(0x00C1);
						i=0;
				}
				break;
			case 3:
					area=EEPROM_read_byte(20);
					if(area == 0)
						sendchar(0x00B0);//CHT
					else
						sendchar(0x00B1);//ENG
					_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();_nop_();	
					sendchar(0x00C1);
					break;
		}	
		
			_nop_();_nop_();_nop_();_nop_();_nop_();		

		}
	


const unsigned char code PH_tab0[11][5] = {
	{17,16,1,0,8}, {17,16,18,9,5}, {17,16,18,9,0}, {17,16,18,8,0}, {17,16,18,7,0},
//       PH 10.8        PH 9.5          PH 9.0          PH 8.0          PH7.0
	{17,16,18,5,5}, {17,16,18,3,5}, {17,16,18,2,5}, {12,20,14,10,19}, {18,18,18,18,18},
//       PH 5.5        PH 3.5          PH 2.5		CLEAn             BLANK
	{17,20,22,5,18}
};//705*/

const unsigned char code PH_tab[11][5] = {
	{17,16,1,0,8}, {17,16,18,9,5}, {17,16,18,9,0}, {17,16,18,8,0}, {17,16,18,7,0},
//       PH 10.8        PH 9.5          PH 9.0          PH 8.0          PH7.0
	{17,16,18,5,5}, {17,16,18,3,5}, {17,16,18,2,5}, {12,20,14,10,19}, {18,18,18,18,18},
//       PH 5.5        PH 3.5          PH SP		CLEAn             BLANK
	{17,20,22,5,18} 
};//705//ACI 3 change from PH2.5 to PH */

const unsigned int code tab_wash[6] = {10, 20, 30, 40, 50, 60};
const unsigned char code tab_voice[8] = {
	4, 3, 2, 1, 17, 6, 7, 5};
const unsigned char code tab_graph[5] = {
	0x02, 0x0B, 0x2D, 0x36, 0x00};

const int code tab_current[8] = {
	1800, 1600, 50, 10, 0, 600, 1200, 5000};

const int code tab_i[8][7] = {
	{1700, 2050, 2400, 2700, 3050, 3400, 3700},
	{ 800, 1150, 1500, 1800, 2150, 2500, 2800},
	{   0,  350,  700, 1000, 1370, 1700, 2000},
	{   0,  200,  400,  600,  950, 1300, 1600},
	{   0,    0,    0,    0,    0,    0,    0},    
	{ 800, 1150, 1500, 1800, 2150, 2500, 2800},
	{1500, 1850, 2200, 2500, 2850, 3200, 3500},
	{2000, 2350, 2700, 3000, 3350, 3700, 4000}
};

//const int code tab_i_fix[8]= {3700, 2800,2000, 1600, 0, 2800, 3500, 4500};//20131227 adjust current without flow
//const int code tab_i_fix[8]= {4200, 1500, 600, 100, 0, 1100, 4900, 5000};//20140106 adjust value after measurement
//const int code tab_i_fix[8]= {5000, 1500, 600, 100, 0, 1100, 4900, 5000};

const int code i_multiply[8] = {100,100,100,100,1,100,100,100};
//unsigned int code iORPValue[8] = {500,400,350,175,150,275,350,650};	 //aki 2 450 change 350	johnson
//unsigned char code iORPValue_index[8] = {100,50,50,75,150,125,100,100};
//signed int code iORPValue[8] = {-390,-330,-315,-260,200,310,530,650};	//20190403 jin
//signed int code iORPValue[8] = {-250,-150,-110, -50,450,520,610,700};	//20190416 jin
  signed int code iORPValue[8] = {-320,-230,-200,-150,300,570,620,700};	//20190416 jin
unsigned char code iORPValue_index[8] = {50,50,50,50,50,50,50,50}; //20190403 jin
unsigned char code iORPValue_Falg[8] = {1,1,1,1,0,0,0,0};
signed int xdata iORPValue_Disp[8] = {0,0,0,0,0,0,0,0}; //20190403 jin



void delay(unsigned int i)
{
	unsigned char j;
	for (; i>0; i--)
		for (j=0; j<255; j++);
}

void Buzzer(unsigned char status)
{
	if(demo_flag == 0)
	{
			if (status)
				VP02 |= 0x20;
			else
				VP02 &= ~0x20;
			P0 = VP02;
			IO_DataLE = 1;
			_nop_();
			_nop_();
				_nop_();
			IO_DataLE=0;
	}
}
void Set_U7_10k(void)
{
	tpl0401a_value = 0;

	if(!TPL0401A_Set_R(tpl0401a_value)) 
		Decrement_R(64);	
}
void U8_switch_short(unsigned char status)
{
	if(demo_flag == 0)
	{
			if (status)
				VP02 |= 0x80;
			else
				VP02 &= ~0x80;
			P0 = VP02;
			IO_DataLE = 1;
			_nop_();
			_nop_();
			_nop_();
			IO_DataLE=0;
	}		
}
void ADS1115_Check(unsigned char status)
{		
	if(!R03_R05N)
	{		
		if(!status)
		{
			ex_ads1115_value=ADS1115_Read(0,1);
			if(ex_ads1115_value>=ADC1115_VE_6P9V)
				POT_Fail_Flag++;
			else
				POT_Fail_Flag=0;
		}
		else 
		{
			if(((Operation_Mode==ELECTROLYSIS_MODE)&&(electrolysis_type !=WFILTER))||((Operation_Mode==CLEAN_MODE)&&(f_clean)))
			{
				if(((tpl0401a_value-ex_tpl0401a_value)>10)||((ex_tpl0401a_value-tpl0401a_value)>10))
				{
					if(((ADS1115_Read(0,1)-ex_ads1115_value)<20)&&(ex_ads1115_value>=ADC1115_VE_6P9V))
					{
						ex_ads1115_value=ADS1115_Read(0,1);
						POT_Fail_Flag++;						
					}
					else
					{
						ex_ads1115_value=ADS1115_Read(0,1);
						ex_tpl0401a_value=tpl0401a_value;
						POT_Fail_Flag=0;
					}
				}
			}
		}	
		if(POT_Fail_Flag>70)
		{
			Operation_Mode=WARNING_MODE;
			ErrCode = 4;			
		}
	}
}
void Acid_Polar(unsigned char status)
{
	if(demo_flag == 0)
	{
			if (status)
				VP02 |= 0x40;
			else
				VP02 &= ~0x40;
			P0 = VP02;
			IO_DataLE = 1;
			_nop_();
			_nop_();
			_nop_();
			IO_DataLE=0;
	}		
}
void voice_12_13(void)//20190326
{
	if(Operation_Mode==STANDBY_MODE)
	{ 
	  PlaySound(Language+PVoice);		// Voice -- above voice
	  PVoice = 0;
	}
	
}
void voice_pvoice()
{
	if(Operation_Mode==ELECTROLYSIS_MODE)
	{ 
	  PlaySound(Language+PVoice_tmp);		// Voice -- above voice
	  PVoice = 0;
	}
}
void ex_int0(void) interrupt 0
{
	FLOW_cnt++;	
}

void time0(void) interrupt 1 // T=1/16 sec 
{
	TR0 = 0;
	T1_cnt++;
	if ((T1_cnt%8)==0)
		Get_ADC_Value();
	TH0 = 0x0B;
	TL0 = 0xDC;
	TR0 = 1;
}

void time1(void) interrupt 3 // T=1/16 sec 
{
	TR1 = 0;
	T0_cnt++;
	
if(Voice_timer_flag)//20190326 jin
{
	
	if (Voice_Timer!=0)     Voice_Timer--; //20190326 jin
	if (Voice_Timer==0)   
	{
		Voice_timer_flag=0;
		voice_12_13();
	}
}
if(Voice_timer_flag_2)//20190411 jin
{
	
	if (Voice_Timer_2!=0)     Voice_Timer_2--; //20190411 jin
	if (Voice_Timer_2==0)   
	{
		Voice_timer_flag_2=0;
		voice_pvoice();
	}
}
	if (Soft_Timer[4]!=0)		Soft_Timer[4]--;
	if (Soft_Timer[6]!=0)		Soft_Timer[6]--;
	
	if ((T0_cnt%8)==0)
		f_half_sec = 1;
			
	if (T0_cnt == 16)
	{
		T0_cnt = 0;
		if (Soft_Timer[0]!=0)			Soft_Timer[0]--;	
		if (Soft_Timer[1]!=0)			Soft_Timer[1]--;	
		if (Soft_Timer[2]!=0)			Soft_Timer[2]--;	
		if (Soft_Timer[3]!=0)			Soft_Timer[3]--;	
		if (Soft_Timer[5]!=0)			Soft_Timer[5]--;
		if (Soft_Timer[7]!=0)			Soft_Timer[7]--;
		if (Soft_Timer_PJ6000[0]!=0)	
		{
			Soft_Timer_PJ6000[0]--;
			sendchar((Soft_Timer_PJ6000[0]&0x000F)+0x50);			
			LCM_Disp_PJ6000_Countdown(Soft_Timer_PJ6000[0]);
		}
		if (Soft_Timer_PJ6000[1]!=0) Soft_Timer_PJ6000[1]--;		
		
	}
	TH1 = 0x0B;
	TL1 = 0xDC;
	TR1 = 1;
}

void T0_Init(void)
{
	TR0 = 0;
	TMOD = 0x11;
	TH0 = 0x0B;
	TL0 = 0xDC;
	T0_cnt = 0;
	ET0 = 1;
	TR0 = 1;
}

void T1_Init(void)
{
	TR1 = 0;
	TMOD = 0x11;
	TH1 = 0x0B;
	TL1 = 0xDC;
	T1_cnt = 0;
	Init_ADC();
	ET1 = 1;
	TR1 = 1;
}

void Water_In(unsigned char status)
{
	if(demo_flag == 0)
	{
			if (status)
			{
				VP02 |= 0x10;
				if(Outwater_Counter>=4294967295)
				{
						overflow_flag|=Over_OUT_C;
						Outwater_Counter=0;
				}					
				Outwater_Counter++;
			}	
			else
				VP02 &= ~0x10;
			P0 = VP02;
			IO_DataLE = 1;
			_nop_();
			_nop_();
			_nop_();
			IO_DataLE=0;
	}		
}

void INT0_Init(void)
{
	INT0 = 1;
	IE0  =0;
	IT0 = 1;
	EX0 = 1;
}

unsigned char Filter_time_is_up(void)
{
	unsigned int i, reti;

	reti = 0;
	KX |= 0x1F0;
	i = UseHour/24;
	if (i >= 73)	CLR_K9();
	if (i >= 146)	CLR_K8();
	if (i >= 219)	CLR_K7();
	if (i >= 292)	CLR_K6();
	if (i >= 365)
	{
		CLR_K5();
		reti = 1;
	}
	return(reti);	 
}

void Disp_Alak_withoutLmin(unsigned char c)
{
	SET_ALKA();
	CLR_FILTER();
	LCM_Disp_Acid(0);
	CLR_L_MIN();
	SET_SP12();
	LCM_Disp_PH_and_R(PH_tab[4-c]);
	LCM_Disp_Alka(c);
	if (! f_alarm_no_water)
		LCM_Backlight(5-c);
	if (Operation_Mode == ELECTROLYSIS_MODE)
		LED_Disp ((5-c)|0x10);
	else
		LED_Disp (5-c);
}

void Disp_Acid_withoutLmin(unsigned char c)
{
	CLR_FILTER();
	CLR_ALKA();
	CLR_L_MIN();
	if(c==3&&P41==1)
		CLR_SP12();
	else
		SET_SP12();
	LCM_Disp_Alka(0);
	LCM_Disp_Acid(c);
	//LCM_Disp_PH_and_R(PH_tab[c+4]);
	//Ken->
	if (P41==0) //
		LCM_Disp_PH_and_R(PH_tab0[c+4]);
	else
		LCM_Disp_PH_and_R(PH_tab[c+4]);
	//<-Ken
	if (! f_alarm_no_water)
		LCM_Backlight(c+5);
	LED_Disp (c+5);
	if (Operation_Mode == ELECTROLYSIS_MODE)
		LED_Disp ((c+5)|0x10);
	else
		LED_Disp (c+5);
}


void Disp_Alak(unsigned char c)
{
	SET_ALKA();
	CLR_FILTER();
	LCM_Disp_Acid(0);
	SET_L_MIN();
	SET_SP12();
	LCM_Disp_PH_and_R(PH_tab[4-c]);
	LCM_Disp_Alka(c);
	if (! f_alarm_no_water)
		LCM_Backlight(5-c);
	if (Operation_Mode == ELECTROLYSIS_MODE)
		LED_Disp ((5-c)|0x10);
	else
		LED_Disp (5-c);
}

void Disp_Filter(void)
{
	SET_FILTER();
	CLR_ALKA();
	SET_L_MIN();
	SET_SP12();
	LCM_Disp_Acid(0);
	LCM_Disp_Alka(0);
	LCM_Disp_PH_and_R(PH_tab[4]);
	if (! f_alarm_no_water)
		LCM_Backlight(BL_Green);
	if (Operation_Mode == ELECTROLYSIS_MODE)
		LED_Disp (0x15);
	else
		LED_Disp (0x05);
}

void Disp_Clean(unsigned char c)
{
	CLR_FILTER();
	CLR_ALKA();
	CLR_SP12();
	LCM_Disp_Acid(0);
	LCM_Disp_Alka(0);
	LCM_Disp_PH_and_R(PH_tab[8]);
	if (c)
		LCM_Backlight(BL_Orange);
	else
		LCM_Backlight(BL_Green);
}

void Disp_Acid(unsigned char c)
{
	CLR_FILTER();
	CLR_ALKA();
	SET_L_MIN();
	if(c==3&&P41==1)
		CLR_SP12();
	else
		SET_SP12();
	LCM_Disp_Alka(0);
	LCM_Disp_Acid(c);
	//LCM_Disp_PH_and_R(PH_tab[c+4]);
	//Ken->
	if (P41==0) //
		LCM_Disp_PH_and_R(PH_tab0[c+4]);
	else
		LCM_Disp_PH_and_R(PH_tab[c+4]);
	//<-Ken
	if (! f_alarm_no_water)
		LCM_Backlight(c+5);
	LED_Disp (c+5);
	if (Operation_Mode == ELECTROLYSIS_MODE)
		LED_Disp ((c+5)|0x10);
	else
		LED_Disp (c+5);
}
void Init_Volt_Detect(void)
{
	r_step = 0;
	r_step_max = 0;
	r_step_cnt = 0;
	Set_U7_10k();
	delay(1000);
}
/**********************************************
 same problem from here
***********************************************/
void SetCurrent(int current)		// current in mA
{
	int d[4];
	
	if(demo_flag == 0)
	{
		
		//	if(current==100)
		//      current=80; //20190306 jin
			
			
		
			d[0] = d[1] = d[2] = d[3] = current/4;

			DAC_write(0, d[0]);
			delay(100);
			DAC_write(1, d[1]);
			delay(100);
			DAC_write(2, d[2]);
			delay(100);
			DAC_write(3, d[3]);

			Init_Volt_Detect();
		}		
}


void EEPROM_write_word(unsigned char addr, unsigned int intdata)
{
	EEPROM_write_byte(addr++, (unsigned char)(intdata));
	EEPROM_write_byte(addr, (unsigned char)(intdata>>8));
}

void EEPROM_write_hide_info(void)
{
	EEPROM_write_word(39, Usage_Counter[0]);
	EEPROM_write_word(41, Usage_Counter[0]>>16);
	EEPROM_write_word(43, Usage_Counter[1]);
	EEPROM_write_word(45, Usage_Counter[1]>>16);
	EEPROM_write_word(47, Usage_Counter[2]);
	EEPROM_write_word(49, Usage_Counter[2]>>16);
	EEPROM_write_word(51, Usage_Counter[3]);
	EEPROM_write_word(53, Usage_Counter[3]>>16);
	EEPROM_write_word(55, Usage_Counter[4]);
	EEPROM_write_word(57, Usage_Counter[4]>>16);
	EEPROM_write_word(59, Usage_Counter[5]);
	EEPROM_write_word(61, Usage_Counter[5]>>16);
	EEPROM_write_word(63, Usage_Counter[6]);
	EEPROM_write_word(65, Usage_Counter[6]>>16);
	EEPROM_write_word(67, Usage_Counter[7]);
	EEPROM_write_word(69, Usage_Counter[7]>>16);
	EEPROM_write_word(71, electrolyze_time);
	EEPROM_write_word(73, electrolyze_time>>16);
	EEPROM_write_word(75, Outwater_Counter);
	EEPROM_write_word(77, Outwater_Counter>>16);
	EEPROM_write_word(79, overflow_flag);	
}

unsigned int EEPROM_read_word(unsigned char addr)
{
	unsigned int rdata;

	rdata = (unsigned int)(EEPROM_read_byte(addr+1));
	rdata <<= 8;
	rdata += (unsigned int)(EEPROM_read_byte(addr));

	return(rdata);	
}
/*************************************************************/
void Disp_electrolysis_type(unsigned char type)
{
	switch (type)
	{
		case 1:	Disp_Alak(4);			break;
		case 2:	Disp_Alak(3);			break;
		case 3:	Disp_Alak(2);			break;
		case 4:	Disp_Alak(1);			break;
		case 5:	Disp_Filter();			break;
		case 6:	Disp_Acid(1);			break;
		case 7:	Disp_Acid(2);			break;
		case 8:	Disp_Acid(3);			break;
	}
}


void Disp_electrolysis_type_withoutLmin(unsigned char type)
{
	switch (type)
	{
		case 1:	Disp_Alak_withoutLmin(4);			break;
		case 2:	Disp_Alak_withoutLmin(3);			break;
		case 3:	Disp_Alak_withoutLmin(2);			break;
		case 4:	Disp_Alak_withoutLmin(1);			break;
		case 6:	Disp_Acid_withoutLmin(1);			break;
		case 7:	Disp_Acid_withoutLmin(2);			break;
		case 8:	Disp_Acid_withoutLmin(3);			break;
	}
}

void Disp_test(void)
{
	char i;
	for (i=0; i<8; i++)//705: i<8 ; 505: i<7
	{
		while(! f_half_sec);
		f_half_sec = 0;
		LED_Disp((i+1)|0x10);
		LCM_Backlight(i+1);
	}
}

void EEPROM_ELECTROLYSIS_WriteDefault(unsigned tmp)
{
	
	EEPROM_write_byte(31+tmp, ele_set[tmp]);	// total current add
	if(tmp>3)
	{
		I_add[tmp+1] = EEPROM_read_byte(31+tmp);
		LCM_Disp_I_add(I_add[tmp+1], 1);
	}
	else
	{
		I_add[tmp] = EEPROM_read_byte(31+tmp);	
		LCM_Disp_I_add(I_add[tmp], 1);
	}
}

void EEPROM_WriteDefault(void)
{
	EEPROM_write_byte(18, 0);
	EEPROM_write_byte(19, 2);	//20190308 jin
	EEPROM_write_byte(20, DEFAULT_LANGUAGE);
	EEPROM_write_byte(30, 20);	// total current add
	EEPROM_write_byte(31, ele_set[0]);	// Alka 4 current add 
	EEPROM_write_byte(32, ele_set[1]);	// Alka 3 current add 
	EEPROM_write_byte(33, ele_set[2]);	// Alka 2 current add 
	EEPROM_write_byte(34, ele_set[3]);	// Alka 1 current add 
	EEPROM_write_byte(35, ele_set[4]);	// Acid 1 current add 
	EEPROM_write_byte(36, ele_set[5]);	// Acid 2 current add 
	EEPROM_write_byte(37, ele_set[6]);	// Acid 3 current add	
	EEPROM_write_byte(38, ~Choose_Default); 
}

void EEPROM_WriteHideData(void)
{
	unsigned char i;
	for(i=39;i<81;i++)	
		EEPROM_write_byte(i, 0);

	
}

void EEPROM_ELECTROLYSIS_Read(void)
{
	I_add_t = EEPROM_read_byte(30);// total current add
	I_add[0] = EEPROM_read_byte(31);// Alka 4 current add 
	I_add[1] = EEPROM_read_byte(32);// Alka 3 current add
	I_add[2] = EEPROM_read_byte(33);// Alka 2 current add
	I_add[3] = EEPROM_read_byte(34);// Alka 1 current add
	I_add[4] = 0;
	I_add[5] = EEPROM_read_byte(35);// Acid 1 current add
	I_add[6] = EEPROM_read_byte(36);// Acid 2 current add
	I_add[7] = EEPROM_read_byte(37);// Acid 3 current add
}
void do_reset(void)
{
	unsigned char c=0, key;
	ex_op_mode=Operation_Mode;
	Clear_key_delay();	
	LCM_Backlight(BL_Green);
	LCM_Fill();
	Soft_Timer[4] = 16*5;
	key = Get_Key();
	while (key)
	key = Get_Key();
	
	while (Soft_Timer[4])
	{
		if(Control_Selection == 0)
			key = __KEY_ENTER__;
		else
			key = Get_Key();	
		if (key == __KEY_ENTER__)
		{
			PlaySound(32);
			electrolysis_type = WALKA3;
			EEPROM_WriteDefault();	
			EEPROM_ELECTROLYSIS_Read();
			Language = DEFAULT_LANGUAGE*32;			
			WashLiter = DEFAULT_WASHLITER;
			Volume=2;
			PlaySound(0x30+Volume);
			f_lock=0;
			CLR_K3();
			EEPROM_write_byte(18, f_lock); 
			EEPROM_write_byte(19, ((WashLiter/10)-1));
			EEPROM_write_byte(20, DEFAULT_LANGUAGE);
		   	LCM_Fill();
			Disp_test();
	     	delay(500);
			break;
		}
		if ((Soft_Timer[4]/10)%2)
			LCM_Fill();	
		else
			LCM_Cls();			
	}
	
	SET_X2();		SET_X3();		CLR_X4();
	LCM_Disp_VOL_K2K3();
	Operation_Mode = STANDBY_MODE;
	LCM_Cls();	
}
void clean_w_PJ6000(void)
{
	unsigned char status=0,finish_flag=0;
	unsigned int total_time=0;
	Soft_Timer_PJ6000[0]=PJ6000_TOTAL_CLEAN_TIME;
	
	INT = 1;
	LCM_Disp_PJ6000();
	LED_Disp(WFILTER | 0x10);
	SendPJ_6000_Time();
	while(Soft_Timer_PJ6000[0])
	{		
		if(Soft_Timer_PJ6000[0]>=(PJ6000_TOTAL_CLEAN_TIME-PJ6000_1_CLEAN_TIME))
			Water_In(1);
		else if(Soft_Timer_PJ6000[0]>=(PJ6000_TOTAL_CLEAN_TIME-(PJ6000_1_CLEAN_TIME+PJ6000_2_CLEAN_TIME)))
		{			
			if(!Soft_Timer_PJ6000[1]) Soft_Timer_PJ6000[1]=124;
			if(Soft_Timer_PJ6000[1]>4)
				Water_In(0);
			else
				Water_In(1);				
		}
		else 
			Water_In(1);					
	}	
	Water_In(0);
	LCM_Disp_KL(MAX_LITER-Liter, 0);		// 顯示濾芯壽命
	INT = 0;
}


void	do_standby(void)
{
	unsigned char key=0,tmp=0,tmp1=0,Set_exit_1=0,Set_exit_2=0;
	unsigned char rr1=0, rr2=0, rr=0;
	ReceiveDate = 0;
	INT = 0;
	f_alarm_no_water = 0;
	LeftLiter = MAX_LITER-Liter;
	Clear_key_delay();	
	key_delay[__KEY_PS_KEY__-1] = 5;
	key_delay[__KEY_MENU__-1] = 5;
	key_delay[__KEY_LOCK__-1] = 3;
	//key_delay[__KEY_RESET__-1] = 5;
	
	//--------------------------------------
	// LCD & LED disp
	Disp_electrolysis_type(electrolysis_type);

	if (f_lock)
		SET_K3();
	else
		CLR_K3();
	//========== 20190312 jin ===========================
	if (Volume==0)
				{
					CLR_X2();		CLR_X3();		CLR_X4();
				}
				if (Volume==1)
				{
					SET_X2();		CLR_X3();		CLR_X4();
				}
				if (Volume==2)
				{
					SET_X2();		SET_X3();		CLR_X4();
				}
				if (Volume==3)
				{
					SET_X2();		SET_X3();		SET_X4();
				}
	
	
	//=================================================
	
	LCM_Disp_VOL_K2K3();
	Filter_time_is_up();
	LCM_Disp_KL(MAX_LITER-Liter, 0);
	LCM_Disp_WaterFlow(0);
	POT_Fail_Flag=0;
	if(Control_Selection == 0)
	{
		ADS1115_Check_flag=0;
		ex_op_mode=Operation_Mode;
	}	
	else if((ex_op_mode==CLEAN_MODE)||(ex_op_mode==ELECTROLYSIS_MODE))
	{
		Soft_Timer[7]=5;
		ex_op_mode=Operation_Mode;
	}
	else
		ex_op_mode=Operation_Mode;
	//--------------------------------------
	while (Operation_Mode == STANDBY_MODE)
	{
		Start_Hour_Count();
		if(((!Soft_Timer[7])&&(Control_Selection))||((!Control_Selection)&&(ADS1115_Check_flag))) ADS1115_Check(0);
		if(!Soft_Timer[5])Water_In(0);
		if (((LeftLiter < 900)||(UseHour > 7884)) && (after_action == 1))		// 濾芯剩10%  9000x0.1=900   73*5*24*0.9
		{
			/*	delay(4000);
			  if(Language==(LAN_FRENCH*32)  )//20190325 jin
				  delay(6000);//20190325 jin
				if( Language==(LAN_CZECH*32)  )
					delay(10000);//20190325 jin
			
			 if(do_clean_flag==1)
				{
          do_clean_flag=0;
					delay(8000);				 
					delay(8000);//add delay 2sec 20190318 jin
					
				}
#define LAN_CHINESE 0
#define LAN_ENGLISH 2
#define LAN_CZECH 3
#define LAN_ITALY 4
#define LAN_FRENCH 5
			 
				*/
			  
			
			 if(do_clean_flag==1)
				{
          do_clean_flag=0;
					Voice_Timer+=96;// =5 sec
					
				}
        else
				{
					 Voice_Timer=16;// =1 sec
				
					if(Language==(LAN_FRENCH*32) || Language==(LAN_ITALY*32) )//20190325 jin
				    Voice_Timer+=24;// =1.5 sec
				  if( Language==(LAN_CZECH*32)  )
					  Voice_Timer+=40;// =2.5 sec
				}
				
				if ((LeftLiter == 0) || (UseHour >= MAX_HOUR))
						PVoice = 13;		// voice 請更換濾芯
				else
						PVoice = 12;		// voice 請準備更換濾芯
					
				Voice_timer_flag=1;// start voice timer
		    after_action=0;
			//	PlaySound(Language+PVoice);		// Voice -- above voice
			//	PVoice = 0;
			//	after_action = 0;
		}
		else
			after_action = 0;		
		if(Control_Selection == 0)
		{
			key = ReceiveDate;
			switch(key)
			{
				case __KEY_FAUCET_SLEEP__:
					ReceiveDate = 0;
					ADS1115_Check_flag=1;
					break;
				
				case __KEY_CLEAN_WITH_PJ6000__:
					ReceiveDate = 0;
					PlaySound(32);
					clean_w_PJ6000();
					break;
				
				case __KEY_VOL_3__:
					ReceiveDate = 0;
					Volume=3;
					PlaySound(0x30+Volume);
					SET_X2();		SET_X3();		SET_X4();
					LCM_Disp_VOL_K2K3();
					PlaySound(32);
					break;
					
				case __KEY_VOL_2__:
					ReceiveDate = 0;
					Volume=2;
					PlaySound(0x30+Volume);
					SET_X2();		SET_X3();		CLR_X4();
					LCM_Disp_VOL_K2K3();
					PlaySound(32);
					break;	

				case __KEY_VOL_1__:
					ReceiveDate = 0;
					Volume=1;
					PlaySound(0x30+Volume);
					SET_X2();		CLR_X3();		CLR_X4();
					LCM_Disp_VOL_K2K3();
					PlaySound(32);
					break;	
								
				case __KEY_VOL_0__:
					ReceiveDate = 0;
					Volume=0;
					PlaySound(0x30+Volume);
					CLR_X2();		CLR_X3();		CLR_X4();
					LCM_Disp_VOL_K2K3();
					break;	

				case __KEY_LAG_C__:
					ReceiveDate = 0;
					PlaySound(32);
					Language = LAN_CHINESE;
					EEPROM_write_byte(20, LAN_CHINESE);
					EEPROM_write_byte(38, LAN_CHINESE);
					break;					
				
				case __KEY_LAG_C_N__:
					ReceiveDate = 0;
					PlaySound(32);
					Language = LAN_CHINESE;
					EEPROM_write_byte(20, LAN_CHINESE);
					EEPROM_write_byte(38, Area_Cht_N);
					break;					

				case __KEY_LAG_C_M__:
					ReceiveDate = 0;
					PlaySound(32);
					Language = LAN_CHINESE;
					EEPROM_write_byte(20, LAN_CHINESE);
					EEPROM_write_byte(38, Area_Cht_M);
					break;					

				case __KEY_LAG_C_S__:
					ReceiveDate = 0;
					PlaySound(32);
					Language = LAN_CHINESE;
					EEPROM_write_byte(20, LAN_CHINESE);
					EEPROM_write_byte(38, Area_Cht_S);
					break;			
				
				case __KEY_LAG_C_E__:
					ReceiveDate = 0;
					PlaySound(32);
					Language = LAN_CHINESE;
					EEPROM_write_byte(20, LAN_CHINESE);
					EEPROM_write_byte(38, Area_Cht_E);
					break;				

				case __KEY_LAG_E__:
					ReceiveDate = 0;
					PlaySound(32);
					Language = LAN_ENGLISH*32;
					EEPROM_write_byte(20, LAN_ENGLISH);		
					EEPROM_write_byte(38, Area_Eng);		
					break;	
				
				case __KEY_LAN_LOOP__:
						ReceiveDate = 0;
						Language = LAN_ENGLISH*32;
						EEPROM_write_byte(20, LAN_ENGLISH);
						EEPROM_write_byte(38, Area_Eng);
						while((ReceiveDate!=__KEY_BACK__)&&(ReceiveDate!=__KEY_PENDING_STATUS__))
						{
							if((ReceiveDate)&&(ReceiveDate!=__KEY_LAN_LOOP__))
							{								
								EEPROM_write_byte(38, ReceiveDate);	
								ReceiveDate=0;
								PlaySound(32);											
							}
						}
							break;
				case __KEY_BACK__:		
					ReceiveDate = 0;
					LCM_Disp_WaterFlow(0);
					Disp_electrolysis_type(electrolysis_type);
					PlaySound(33);
					break;		
					
				case __KEY_PENDING_STATUS__:
					ReceiveDate = 0;
					LCM_Disp_WaterFlow(0);
					Disp_electrolysis_type(electrolysis_type);
					break;						
				
				case __KEY_DIN_SOUND__:		
					ReceiveDate = 0;
					PlaySound(32);
					break;	

				case __KEY_SET_ELE__:
				{
					ReceiveDate = 0;
					PlaySound(33);
					Set_exit_2 = 0;
					while(Set_exit_2 == 0)
					{
							key = ReceiveDate;
							
							switch(key)
							{
								case __KEY_DIN_SOUND__:		
											ReceiveDate = 0;
											PlaySound(32);
											break;			
								
								case __KEY_SET_ELE_ALK4__:
											ReceiveDate = 0;
											key = ReceiveDate;
											tmp=0;
											tmp1 = EEPROM_read_byte(31+tmp);
											sendchar(tmp1);
											SET_SP12(); CLR_L_MIN();
											Disp_electrolysis_type(tmp+1);																		
											LCM_Disp_I_add(tmp1, 1);
											//PlaySound(32);
											Set_exit_1=0;
											while(! Set_exit_1)
										{
											key = ReceiveDate;
											switch(key)
											{
											case __KEY_LOCK__:
												ReceiveDate = 0;
												if(tmp1 < 50)
												{
													tmp1++;
													PlaySound(32);	
												}	
												sendchar(tmp1);
												LCM_Disp_I_add(tmp1, 1);												
												break;
												
												case __KEY_VOL_ADJ__:
												ReceiveDate = 0;
												if(tmp1 > 0)
												{
													tmp1--;
													PlaySound(32);
												}
												sendchar(tmp1);
												LCM_Disp_I_add(tmp1, 1);												
												break;

											case __KEY_SET_ELE_ALK4__:
												ReceiveDate = 0;
												tmp=0;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);
												//Set_exit_1 = 1;
												//PlaySound(32);	
												break;												

											case __KEY_SET_ELE_ALK3__:
												ReceiveDate = 0;
												tmp=1;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_ALK2__:
												ReceiveDate = 0;
												tmp=2;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;

											case __KEY_SET_ELE_ALK1__:
												ReceiveDate = 0;
												tmp=3;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;												

											case __KEY_SET_ELE_ACI1__:
												ReceiveDate = 0;
												tmp=4;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);			
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_ACI2__:
												ReceiveDate = 0;
												tmp=5;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);				
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;											

											case __KEY_SET_ELE_ACI3__:
												ReceiveDate = 0;
												tmp=6;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);				
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_RST__:
												ReceiveDate = 0;
												EEPROM_ELECTROLYSIS_WriteDefault(tmp);
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												PlaySound(32);
												break;
											
											case __KEY_BACK__:		
												ReceiveDate = 0;
												Set_exit_1 = 1;	
												Set_exit_2 = 1;
												LCM_Disp_WaterFlow(0);
												Disp_electrolysis_type(electrolysis_type);
												EEPROM_ELECTROLYSIS_Read();
												PlaySound(33);
												break;
											
											case __KEY_PENDING_STATUS__:
												ReceiveDate = 0;
												Set_exit_1 = 1;	
												Set_exit_2 = 1;
												LCM_Disp_WaterFlow(0);
												Disp_electrolysis_type(electrolysis_type);
												EEPROM_ELECTROLYSIS_Read();
												break;
								
											case __KEY_DIN_SOUND__:		
												ReceiveDate = 0;
												PlaySound(32);
												break;											
											
											}									
											EEPROM_write_byte(31+tmp, tmp1);
										}
										break;
										
									case __KEY_SET_ELE_ALK3__:
											ReceiveDate = 0;
											key = ReceiveDate;
											tmp=1;
											tmp1 = EEPROM_read_byte(31+tmp);
											SET_SP12(); CLR_L_MIN();
											Disp_electrolysis_type(tmp+1);																			
											LCM_Disp_I_add(tmp1, 1);
											sendchar(tmp1);
											Set_exit_1=0;			
											//PlaySound(32);
											while(! Set_exit_1)
										{
											key = ReceiveDate;
											switch(key)
											{
											case __KEY_LOCK__:
												ReceiveDate = 0;
												if(tmp1 < 50)
												{
													tmp1++;
													PlaySound(32);	
												}	
												sendchar(tmp1);
												LCM_Disp_I_add(tmp1, 1);												
												break;
												
												case __KEY_VOL_ADJ__:
												ReceiveDate = 0;
												if(tmp1 > 0)
												{
													tmp1--;
													PlaySound(32);
												}
												sendchar(tmp1);
												LCM_Disp_I_add(tmp1, 1);												
												break;

											case __KEY_SET_ELE_ALK4__:
												ReceiveDate = 0;
												tmp=0;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;												

											case __KEY_SET_ELE_ALK3__:
												ReceiveDate = 0;
												tmp=1;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												//Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_ALK2__:
												ReceiveDate = 0;
												tmp=2;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;

											case __KEY_SET_ELE_ALK1__:
												ReceiveDate = 0;
												tmp=3;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;												

											case __KEY_SET_ELE_ACI1__:
												ReceiveDate = 0;
												tmp=4;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);			
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_ACI2__:
												ReceiveDate = 0;
												tmp=5;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);				
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;											

											case __KEY_SET_ELE_ACI3__:
												ReceiveDate = 0;
												tmp=6;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);				
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_RST__:
												ReceiveDate = 0;
												EEPROM_ELECTROLYSIS_WriteDefault(tmp);
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												PlaySound(32);
												break;
											
											case __KEY_PENDING_STATUS__:
												ReceiveDate = 0;
												Set_exit_1 = 1;	
												Set_exit_2 = 1;
												LCM_Disp_WaterFlow(0);
												Disp_electrolysis_type(electrolysis_type);
												EEPROM_ELECTROLYSIS_Read();
												break;
																						
											case __KEY_BACK__:		
												ReceiveDate = 0;
												Set_exit_1 = 1;	
												Set_exit_2 = 1;
												LCM_Disp_WaterFlow(0);
												Disp_electrolysis_type(electrolysis_type);
												PlaySound(33);
												break;

											case __KEY_DIN_SOUND__:		
												ReceiveDate = 0;
												PlaySound(32);
												break;												
											}									
											EEPROM_write_byte(31+tmp, tmp1);
										
										}	
										break;
									
									case __KEY_SET_ELE_ALK2__:
											ReceiveDate = 0;
											key = ReceiveDate;
											tmp=2;
											tmp1 = EEPROM_read_byte(31+tmp);
											SET_SP12(); CLR_L_MIN();
											Disp_electrolysis_type(tmp+1);																			
											LCM_Disp_I_add(tmp1, 1);
											sendchar(tmp1);
											Set_exit_1=0;
											//PlaySound(32);
											while(! Set_exit_1)
										{
											key = ReceiveDate;
											switch(key)
											{
											case __KEY_LOCK__:
												ReceiveDate = 0;
												if(tmp1 < 50)
												{
													tmp1++;
													PlaySound(32);	
												}	
												sendchar(tmp1);
												LCM_Disp_I_add(tmp1, 1);												
												break;
												
												case __KEY_VOL_ADJ__:
												ReceiveDate = 0;
												if(tmp1 > 0)
												{
													tmp1--;
													PlaySound(32);
												}
												sendchar(tmp1);
												LCM_Disp_I_add(tmp1, 1);												
												break;

											case __KEY_SET_ELE_ALK4__:
												ReceiveDate = 0;
												tmp=0;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;												

											case __KEY_SET_ELE_ALK3__:
												ReceiveDate = 0;
												tmp=1;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_ALK2__:
												ReceiveDate = 0;
												tmp=2;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												//Set_exit_1 = 1;	
												//PlaySound(32);
												break;

											case __KEY_SET_ELE_ALK1__:
												ReceiveDate = 0;
												tmp=3;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;												

											case __KEY_SET_ELE_ACI1__:
												ReceiveDate = 0;
												tmp=4;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);			
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_ACI2__:
												ReceiveDate = 0;
												tmp=5;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);				
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;											

											case __KEY_SET_ELE_ACI3__:
												ReceiveDate = 0;
												tmp=6;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);				
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;

											case __KEY_SET_ELE_RST__:
												ReceiveDate = 0;
												EEPROM_ELECTROLYSIS_WriteDefault(tmp);
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												PlaySound(32);
												break;
											
											case __KEY_PENDING_STATUS__:
												ReceiveDate = 0;
												Set_exit_1 = 1;	
												Set_exit_2 = 1;
												LCM_Disp_WaterFlow(0);
												Disp_electrolysis_type(electrolysis_type);
												EEPROM_ELECTROLYSIS_Read();
												break;
																						
											case __KEY_BACK__:		
												ReceiveDate = 0;
												Set_exit_1 = 1;	
												Set_exit_2 = 1;
												LCM_Disp_WaterFlow(0);
												Disp_electrolysis_type(electrolysis_type);
												PlaySound(33);
												break;
											
											case __KEY_DIN_SOUND__:		
												ReceiveDate = 0;
												PlaySound(32);
												break;												
											}									
											EEPROM_write_byte(31+tmp, tmp1);
										}
										break;
										
									case __KEY_SET_ELE_ALK1__:
											ReceiveDate = 0;
											key = ReceiveDate;
											tmp=3;
											tmp1 = EEPROM_read_byte(31+tmp);
											SET_SP12(); CLR_L_MIN();
											Disp_electrolysis_type(tmp+1);																			
											LCM_Disp_I_add(tmp1, 1);
											sendchar(tmp1);
											Set_exit_1=0;
											//PlaySound(32);
											while(! Set_exit_1)
										{
											key = ReceiveDate;
											switch(key)
											{
											case __KEY_LOCK__:
												ReceiveDate = 0;
												if(tmp1 < 50)
												{
													tmp1++;
													PlaySound(32);	
												}	
												sendchar(tmp1);
												LCM_Disp_I_add(tmp1, 1);												
												break;
												
												case __KEY_VOL_ADJ__:
												ReceiveDate = 0;
												if(tmp1 > 0)
												{
													tmp1--;
													PlaySound(32);
												}
												sendchar(tmp1);
												LCM_Disp_I_add(tmp1, 1);												
												break;

											case __KEY_SET_ELE_ALK4__:
												ReceiveDate = 0;
												tmp=0;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;												

											case __KEY_SET_ELE_ALK3__:
												ReceiveDate = 0;
												tmp=1;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_ALK2__:
												ReceiveDate = 0;
												tmp=2;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;

											case __KEY_SET_ELE_ALK1__:
												ReceiveDate = 0;
												tmp=3;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												//Set_exit_1 = 1;	
												//PlaySound(32);
												break;												

											case __KEY_SET_ELE_ACI1__:
												ReceiveDate = 0;
												tmp=4;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);			
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_ACI2__:
												ReceiveDate = 0;
												tmp=5;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);				
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;											

											case __KEY_SET_ELE_ACI3__:
												ReceiveDate = 0;
												tmp=6;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);				
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;
	
											case __KEY_SET_ELE_RST__:
												ReceiveDate = 0;
												EEPROM_ELECTROLYSIS_WriteDefault(tmp);
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												PlaySound(32);
												break;
											
											case __KEY_PENDING_STATUS__:
												ReceiveDate = 0;
												Set_exit_1 = 1;	
												Set_exit_2 = 1;
												LCM_Disp_WaterFlow(0);
												Disp_electrolysis_type(electrolysis_type);
												EEPROM_ELECTROLYSIS_Read();
												break;
																						
											case __KEY_BACK__:		
												ReceiveDate = 0;
												Set_exit_1 = 1;	
												Set_exit_2 = 1;
												LCM_Disp_WaterFlow(0);
												Disp_electrolysis_type(electrolysis_type);
												PlaySound(33);
												break;	

											case __KEY_DIN_SOUND__:		
												ReceiveDate = 0;
												PlaySound(32);
												break;												
											}									
											EEPROM_write_byte(31+tmp, tmp1);
										}
										break;

									case __KEY_SET_ELE_ACI1__:
										ReceiveDate = 0;
										tmp=4;
										tmp1 = EEPROM_read_byte(31+tmp);
										SET_SP12(); CLR_L_MIN();
										Disp_electrolysis_type(tmp+2);																				
										LCM_Disp_I_add(tmp1, 1);
										sendchar(tmp1);
										Set_exit_1=0;
										//PlaySound(32);
										while(! Set_exit_1)
										{
											key = ReceiveDate;
											switch(key)
											{
											case __KEY_LOCK__:
												ReceiveDate = 0;
												if(tmp1 < 50)
												{
													tmp1++;
													PlaySound(32);	
												}	
												sendchar(tmp1);
												LCM_Disp_I_add(tmp1, 1);												
												break;
												
												case __KEY_VOL_ADJ__:
												ReceiveDate = 0;
												if(tmp1 > 0)
												{
													tmp1--;
													PlaySound(32);
												}
												sendchar(tmp1);
												LCM_Disp_I_add(tmp1, 1);												
												break;

											case __KEY_SET_ELE_ALK4__:
												ReceiveDate = 0;
												tmp=0;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;												

											case __KEY_SET_ELE_ALK3__:
												ReceiveDate = 0;
												tmp=1;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_ALK2__:
												ReceiveDate = 0;
												tmp=2;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;

											case __KEY_SET_ELE_ALK1__:
												ReceiveDate = 0;
												tmp=3;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;												

											case __KEY_SET_ELE_ACI1__:
												ReceiveDate = 0;
												tmp=4;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);			
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												//Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_ACI2__:
												ReceiveDate = 0;
												tmp=5;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);				
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;											

											case __KEY_SET_ELE_ACI3__:
												ReceiveDate = 0;
												tmp=6;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);				
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;

											case __KEY_SET_ELE_RST__:
												ReceiveDate = 0;
												EEPROM_ELECTROLYSIS_WriteDefault(tmp);
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												PlaySound(32);
												break;
											
											case __KEY_PENDING_STATUS__:
												ReceiveDate = 0;
												Set_exit_1 = 1;	
												Set_exit_2 = 1;
												LCM_Disp_WaterFlow(0);
												Disp_electrolysis_type(electrolysis_type);
												EEPROM_ELECTROLYSIS_Read();
												break;
											
											case __KEY_BACK__:		
												ReceiveDate = 0;
												Set_exit_1 = 1;	
												Set_exit_2 = 1;
												LCM_Disp_WaterFlow(0);
												Disp_electrolysis_type(electrolysis_type);
												PlaySound(33);
												break;		

											case __KEY_DIN_SOUND__:		
												ReceiveDate = 0;
												PlaySound(32);
												break;												
											}									
											EEPROM_write_byte(31+tmp, tmp1);
										}
										break;								

									case __KEY_SET_ELE_ACI2__:
										ReceiveDate = 0;
										tmp=5;
										tmp1 = EEPROM_read_byte(31+tmp);
										SET_SP12(); CLR_L_MIN();
										Disp_electrolysis_type(tmp+2);																		
										LCM_Disp_I_add(tmp1, 1);
										sendchar(tmp1);
										Set_exit_1=0;
										//PlaySound(32);
										while(! Set_exit_1)
										{
											key = ReceiveDate;
											switch(key)
											{
											case __KEY_LOCK__:
												ReceiveDate = 0;
												if(tmp1 < 50)	
												{
													tmp1++;														
													PlaySound(32);		
												}
												sendchar(tmp1);
												LCM_Disp_I_add(tmp1, 1);
												break;
												
												case __KEY_VOL_ADJ__:
												ReceiveDate = 0;
												if(tmp1 > 0)
												{	
													tmp1--;
													PlaySound(32);		
												}	
												sendchar(tmp1);
												LCM_Disp_I_add(tmp1, 1);												
												break;

											case __KEY_SET_ELE_ALK4__:
												ReceiveDate = 0;
												tmp=0;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;												

											case __KEY_SET_ELE_ALK3__:
												ReceiveDate = 0;
												tmp=1;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_ALK2__:
												ReceiveDate = 0;
												tmp=2;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;
												//PlaySound(32);
												break;

											case __KEY_SET_ELE_ALK1__:
												ReceiveDate = 0;
												tmp=3;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;												

											case __KEY_SET_ELE_ACI1__:
												ReceiveDate = 0;
												tmp=4;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);			
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_ACI2__:
												ReceiveDate = 0;
												tmp=5;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);				
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												//Set_exit_1 = 1;	
												//PlaySound(32);
												break;											

											case __KEY_SET_ELE_ACI3__:
												ReceiveDate = 0;
												tmp=6;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);				
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_RST__:
												ReceiveDate = 0;
												EEPROM_ELECTROLYSIS_WriteDefault(tmp);
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												PlaySound(32);
												break;
											
											case __KEY_PENDING_STATUS__:
												ReceiveDate = 0;
												Set_exit_1 = 1;	
												Set_exit_2 = 1;
												LCM_Disp_WaterFlow(0);
												Disp_electrolysis_type(electrolysis_type);
												EEPROM_ELECTROLYSIS_Read();
												break;
											
											case __KEY_BACK__:		
												ReceiveDate = 0;
												Set_exit_1 = 1;	
												Set_exit_2 = 1;
												LCM_Disp_WaterFlow(0);
												Disp_electrolysis_type(electrolysis_type);
												PlaySound(33);
												break;
											
											case __KEY_DIN_SOUND__:		
												ReceiveDate = 0;
												PlaySound(32);
												break;												
											}									
											EEPROM_write_byte(31+tmp, tmp1);
										}
										break;	

									case __KEY_SET_ELE_ACI3__:
										ReceiveDate = 0;
										tmp=6;
										tmp1 = EEPROM_read_byte(31+tmp);
										sendchar(tmp1);
										SET_SP12(); CLR_L_MIN();
										Disp_electrolysis_type(tmp+2);																			
										LCM_Disp_I_add(tmp1, 1);							
										Set_exit_1=0;
										//PlaySound(32);

										while(! Set_exit_1)
										{
											key = ReceiveDate;
											switch(key)
											{
											case __KEY_LOCK__:
												ReceiveDate = 0;
												if(tmp1 < 50)	
												{	
													tmp1++;
													PlaySound(32);		
												}
												sendchar(tmp1);
												LCM_Disp_I_add(tmp1, 1);														
												break;
												
												case __KEY_VOL_ADJ__:
												ReceiveDate = 0;
												if(tmp1 > 0) 
												{	
													tmp1--;													
													PlaySound(32);		
												}
												sendchar(tmp1);
												LCM_Disp_I_add(tmp1, 1);	
												break;

											case __KEY_SET_ELE_ALK4__:
												ReceiveDate = 0;
												tmp=0;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;												

											case __KEY_SET_ELE_ALK3__:
												ReceiveDate = 0;
												tmp=1;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_ALK2__:
												ReceiveDate = 0;
												tmp=2;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;

											case __KEY_SET_ELE_ALK1__:
												ReceiveDate = 0;
												tmp=3;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+1);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;												

											case __KEY_SET_ELE_ACI1__:
												ReceiveDate = 0;
												tmp=4;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);			
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_ACI2__:
												ReceiveDate = 0;
												tmp=5;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);				
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												Set_exit_1 = 1;	
												//PlaySound(32);
												break;											

											case __KEY_SET_ELE_ACI3__:
												ReceiveDate = 0;
												tmp=6;
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);				
												SET_SP12(); CLR_L_MIN();
												Disp_electrolysis_type(tmp+2);																		
												LCM_Disp_I_add(tmp1, 1);											
												//Set_exit_1 = 1;	
												//PlaySound(32);
												break;
											
											case __KEY_SET_ELE_RST__:
												ReceiveDate = 0;
												EEPROM_ELECTROLYSIS_WriteDefault(tmp);
												tmp1 = EEPROM_read_byte(31+tmp);
												sendchar(tmp1);
												PlaySound(32);
												break;
											
											case __KEY_PENDING_STATUS__:
												ReceiveDate = 0;
												Set_exit_1 = 1;	
												Set_exit_2 = 1;
												LCM_Disp_WaterFlow(0);
												Disp_electrolysis_type(electrolysis_type);
												EEPROM_ELECTROLYSIS_Read();
												break;
											
											
											case __KEY_BACK__:		
												ReceiveDate = 0;
												Set_exit_1 = 1;	
												Set_exit_2 = 1;
												PlaySound(33);
												break;
											
											case __KEY_DIN_SOUND__:		
												ReceiveDate = 0;
												PlaySound(32);
												break;												
											
											}									
											EEPROM_write_byte(31+tmp, tmp1);
										}
										break;										
										
									case __KEY_BACK__:		
										ReceiveDate = 0;
										Set_exit_2 = 1;
										LCM_Disp_WaterFlow(0);
										Disp_electrolysis_type(electrolysis_type);										
										PlaySound(33);
										break;
									
									case __KEY_PENDING_STATUS__:		
										ReceiveDate = 0;
										Set_exit_2 = 1;
										LCM_Disp_WaterFlow(0);
										Disp_electrolysis_type(electrolysis_type);										
										PlaySound(33);
										break;
								}									

					}
				}
					break;
				
				case __KEY_HIDDEN_PAGE__:
					ReceiveDate = 0;
					PlaySound(33);
					while((key != __KEY_BACK__)&&(key != __KEY_PENDING_STATUS__))
					{
						key = ReceiveDate;
						switch(key)
						{
							case __KEY_USAGE_COUNT_ALK4__:
							case __KEY_USAGE_COUNT_ALK3__:	
							case __KEY_USAGE_COUNT_ALK2__:
							case __KEY_USAGE_COUNT_ALK1__:							
							case __KEY_USAGE_COUNT_FILTER__:
							case __KEY_USAGE_COUNT_ACI1__:	
							case __KEY_USAGE_COUNT_ACI2__:
							case __KEY_USAGE_COUNT_ACI3__:	
							case __KEY_ELECTROLYZE_TIME__:
							case __KEY_OUTWATER_COUNT__:								
									SendHideInfo((key-__KEY_USAGE_COUNT_ALK4__));
									break;		
						}
						
					}		
					ReceiveDate = 0;
					break;
				
				case __KEY_INFORMATION__:
					ReceiveDate = 0;
					PlaySound(33);
					while((key != __KEY_BACK__)&&(key != __KEY_DEMO_MODE__)&&(key != __KEY_PENDING_STATUS__))
					{key = ReceiveDate;delay(1000);LCM_Disp_Version(VERSION_Y, VERSION_MD);SendVersion();}
					delay(1000);SendMode(3, 0);
					break;

				case __KEY_CLEAR__:
					ReceiveDate = 0;
					PlaySound(33);
					LeftLiter = 9000;
					SET_K5();
					SET_K6();		
					SET_K7();
					SET_K8();		
					SET_K9();		
					LCM_Disp_KL(LeftLiter, 0);
					left_FlowCnt = 0;		
					Liter = 0;			
					UseMin = 0;			
					UseHour = 0;	
					EEPROM_write_word(21, left_FlowCnt);	// left_FlowCnt on EEPROM address 21~22
					delay(300);
					EEPROM_write_word(23, Liter);	// Liter on EEPROM address 23~24
					delay(300);
					EEPROM_write_word(25, UseMin);	// filter used sec 25~26
					delay(300);
					EEPROM_write_word(27, UseHour);	// filter used hour 27~28
		
					break;	
				
				case __KEY_RESET__:
					ReceiveDate = 0;
					Operation_Mode = RESET_MODE;
					break;
				
				case __KEY_SET_CLEAN_10L__:
					ReceiveDate = 0;
					PlaySound(32);
					tmp=0;
					EEPROM_write_byte(19, tmp);
					WashLiter = tab_wash[tmp];
					LCM_Disp_WashLiter(tab_wash[tmp], 1);
					break;
				
				case __KEY_SET_CLEAN_20L__:
					ReceiveDate = 0;
					PlaySound(32);
					tmp=1;
					EEPROM_write_byte(19, tmp);
					WashLiter = tab_wash[tmp];
					LCM_Disp_WashLiter(tab_wash[tmp], 1);
					break;
				
				case __KEY_SET_CLEAN_30L__:
					ReceiveDate = 0;
					PlaySound(32);
					tmp=2;
					EEPROM_write_byte(19, tmp);
					WashLiter = tab_wash[tmp];
					LCM_Disp_WashLiter(tab_wash[tmp], 1);
					break;
				
				case __KEY_SET_CLEAN_40L__:
					ReceiveDate = 0;
					PlaySound(32);
					tmp=3;
					EEPROM_write_byte(19, tmp);
					WashLiter = tab_wash[tmp];
					LCM_Disp_WashLiter(tab_wash[tmp], 1);
					break;

				case __KEY_SET_CLEAN_50L__:
					ReceiveDate = 0;
					PlaySound(32);
					tmp=4;
					EEPROM_write_byte(19, tmp);
					WashLiter = tab_wash[tmp];
					LCM_Disp_WashLiter(tab_wash[tmp], 1);
					break;
				
				case __KEY_SET_CLEAN_60L__:
					ReceiveDate = 0;
					PlaySound(32);
					tmp=5;
					EEPROM_write_byte(19, tmp);
					WashLiter = tab_wash[tmp];
					LCM_Disp_WashLiter(tab_wash[tmp], 1);
					break;				
			}
		}
		else
			key = Get_Key();
		if (key == 0)
		{
			if (SleepEnable)
			{
				if (! Check_SleepCounter_Start())
					Start_SleepCounter(60);		// 1 minutes, 60=1*60
				else
				{
					if (Is_SleepCounter_CountDown_Finish())
					{
						Stop_SleepCounter();
						Operation_Mode = SLEEP_MODE;
					}
				}
			}
		}
		else
		{
			Stop_SleepCounter();		// if any key is been pressed, stop sleep counter
	
			switch (key)
			{
				case __KEY_MENU__:
					ReceiveDate = 0;
					Operation_Mode = SETUP_MODE;
					break;				

				case __KEY_LOCK__:
					ReceiveDate = 0;
					f_lock = ~f_lock;
					if (f_lock)
					{
						EEPROM_write_byte(18, 0x80+electrolysis_type); 
						SET_K3();
						if(Control_Selection == 0 && Language != LAN_CHINESE)
							PlaySound(32);
						else
							PlaySound(Language+8);
					}
					else
					{
						EEPROM_write_byte(18, 0); 
						CLR_K3();
						if(Control_Selection == 0 && Language != LAN_CHINESE)
							PlaySound(32);
						else
						PlaySound(Language+9);
					}
					LCM_Disp_VOL_K2K3();	// LOCK 圖示		
					break;

				case __KEY_PS_KEY__:
					ReceiveDate = 0;
					//	SleepEnable = (SleepEnable)? 0 : 1;
					PlaySound(32);
					SleepEnable++;
					SleepEnable &= 0x01;
					break;

				case __KEY_ALKA4__:
					ReceiveDate = 0;
					if (! f_lock)
					{
						electrolysis_type = WALKA4;
						Disp_electrolysis_type(electrolysis_type);
					}
					break;
					
					case __KEY_ALKA3__:
					ReceiveDate = 0;
					if (! f_lock)
					{
						electrolysis_type = WALKA3;
						Disp_electrolysis_type(electrolysis_type);
					}
					break;	

				case __KEY_ALKA2__:
					ReceiveDate = 0;
					if (! f_lock)
					{
						electrolysis_type = WALKA2;
						Disp_electrolysis_type(electrolysis_type);
					}
					break;

				case __KEY_ALKA1__:
					ReceiveDate = 0;
					if (! f_lock)
					{
						electrolysis_type = WALKA1;
						Disp_electrolysis_type(electrolysis_type);
					}
					break;
	
				case __KEY_FILTER__:
					ReceiveDate = 0;
					if (! f_lock)
					{
						electrolysis_type = WFILTER;
						Disp_electrolysis_type(electrolysis_type);
					}
					break;
	
				case __KEY_ACID1__:
					ReceiveDate = 0;
					if (! f_lock)
					{
						electrolysis_type = WACID1;
						Disp_electrolysis_type(electrolysis_type);
					}
					break;

				case __KEY_ACID2__:
					ReceiveDate = 0;
					if (! f_lock)
					{
						electrolysis_type = WACID2;
						Disp_electrolysis_type(electrolysis_type);
					}
					break;

				case __KEY_ACID3__:
					ReceiveDate = 0;
					if (! f_lock)
					{
						electrolysis_type = WACID3;
						Disp_electrolysis_type(electrolysis_type);
					}
					break;
				
				case __KEY_VOL_ADJ__:
					ReceiveDate = 0;
					Volume++;
					Volume &= 0x03;
					PlaySound(0x30+Volume);
					if (Volume==0)
					{
						CLR_X2();		CLR_X3();		CLR_X4();
					}
					if (Volume==1)
					{
						SET_X2();		CLR_X3();		CLR_X4();
					}
					if (Volume==2)
					{
						SET_X2();		SET_X3();		CLR_X4();
					}
					if (Volume==3)
					{
						SET_X2();		SET_X3();		SET_X4();
					}
					LCM_Disp_VOL_K2K3();
					PlaySound(32);
					break;

				case __KEY_ACTION__:
					ReceiveDate = 0;
					if(Control_Selection == 0 && Language != LAN_CHINESE)
						PlaySound(34);
					else
					{
						if(Language==LAN_CZECH*32)//20190418 jin
							PlaySound(Language+25);
						else
						  PlaySound(Language+24);  // play voice -- turn on
					}
					if ((electrolysis_type >= WALKA4)&&(electrolysis_type < WACID3)&&(f_acid3))
						Operation_Mode = CLEAN_MODE;
					else
							Operation_Mode = ELECTROLYSIS_MODE;

					LED_Disp(electrolysis_type | 0x10);
					break;
			}

		}
		


		if (f_half_sec)		// 每半秒
		{
			if (LeftLiter > 0)
				rr1 &= ~0x02;
			else
			{
				if (rr2)
					rr1 |= 0x02;
				else
					rr1 &= ~0x02;
			}
			if (! Filter_time_is_up())
				rr1 &= ~0x01;
			else	
			{
				if (rr2)
					rr1 |= 0x01;
				else
					rr1 &= ~0x01;
			}
			rr2++;
			rr2 &= 0x01;
			LCM_Disp_KL(LeftLiter, rr1);
			if (PowerOverHot_warning() || Operation_Mode == WARNING_MODE)
			{
				Operation_Mode = WARNING_MODE;
				break;
			}
			SendMode(2, 0);
			f_half_sec = 0;
		}

	}
}

void Disp_FlowGraph(unsigned char GraphStep, unsigned char *PH_String)
{
	unsigned int iTemp;
	unsigned char ORP_factor, vsp12;

	if (f_show_orp == 1)		// David 2012/08/16
	{
		vsp12 = 0;
		if (SP12)	vsp12 = 1;
		R = tab_graph[GraphStep];
		SET_ORP();
		SET_MV();
/*		if (electrolysis_type == 5)
			iORPValue_Disp[electrolysis_type-1]=iORPValue[electrolysis_type-1];
		else
		{*/
			ORP_factor = (unsigned char)(rand()%200);
			//iTemp = iORPValue[electrolysis_type-1];
			if (ORP_factor >= 100)
			{
				ORP_factor -= 100;
				if(ORP_factor>=90) ORP_factor=100;
				iTemp = ORP_factor*iORPValue_index[electrolysis_type-1]/100;
				iORPValue_Disp[electrolysis_type-1] = iORPValue[electrolysis_type-1]-iTemp;
			}
			else
			{
				if(ORP_factor>=90) ORP_factor=100;
				iTemp = ORP_factor*iORPValue_index[electrolysis_type-1]/100;
				iORPValue_Disp[electrolysis_type-1] = iORPValue[electrolysis_type-1]+iTemp;
			}
		/*}*/
		if (iORPValue_Disp[electrolysis_type-1] > 999)
			iORPValue_Disp[electrolysis_type-1] = 999;
		if(flow_cnt_tmp[1] >= 7)
		{
			if(iORPValue_Disp[electrolysis_type-1]<0)
			   LCM_Disp_ORP(iORPValue_Disp[electrolysis_type-1]*(-1),1);  
			else
				 LCM_Disp_ORP(iORPValue_Disp[electrolysis_type-1],0);
		}
		else
		{
			LCM_Disp_ORP(0                                  ,0);
		}
		if (vsp12)
			SET_SP12();
	}
	else
	{
		R = (SP12)? (tab_graph[GraphStep]|0x80) : tab_graph[GraphStep];
		LCM_Disp_PH_and_R(PH_String);
	}
}

void Volt_Ctrl(void)
{
	static unsigned char adjust_loop=0;
	
	if(1)//R03_R05N) 20190128 jin
	{
		if (V_DETECT)  	//pin = Hi
		{
			if (1)//r_step_cnt < 100)   // &&(r_step < r_step_max))  //push all voltage 20190128 jin
			{
				Increment_R(1);	  // step by step
				r_step++;
				r_step_max++;
			   adjust_loop = 0;
				if(Operation_Mode==CLEAN_MODE)//20190131 for acid clear big current
					delay(1000);
			}
		}
		else
		{
			if (r_step > r_step_max)	//when over then dec by step
			{
//				r_step_max = r_step;
				r_step_cnt = 0;
				Decrement_R(1);
				r_step--;
			}
			else
			{
				adjust_loop++;
				if (r_step == r_step_max)	   ////aaaaa
				{
					r_step_cnt++;
					Decrement_R(1);
					r_step--;
				}
				else if(adjust_loop > 10)
				{
					Decrement_R(1);
					adjust_loop=0;
				}
			}
		}
	}
	else
	{
		ads1115_temp=ADS1115_Read(1,1);
		if (V_DETECT)
		{
			if(tpl0401a_value<128)
			{
				tpl0401a_value=tpl0401a_value+1;	//+3
				if(tpl0401a_value>=128)
				{
					tpl0401a_value=127;
					adjust_loop++;
					if(adjust_loop>20)
						U8_switch_short(1);
				}
			}
			
		}
		else
		{
			adjust_loop=0;
			U8_switch_short(0);
			if(tpl0401a_value>0)
				tpl0401a_value=tpl0401a_value-1;
		}			
			TPL0401A_Set_R(tpl0401a_value);			
	}
	delay(50);
}
//=================================================
void do_clean(unsigned char ele)  
{
	unsigned char rr1=0, rr=0;
	unsigned int LeftLiter;
	unsigned char NoWaterVoice_Counter=0,clean_flow,key,RUN_LED=0,RUN_DER=0,stop_sound_loop=0,vs_stop=0;
	unsigned char Time_of_NoWater=0,count_down_sec=30,count_status=0;
	unsigned int i1=1000; //20190306 jin
	bit rr2=0;
	ReceiveDate = 0;
	Soft_Timer[6]=8;
	EX0 = 0;
	f_half_sec = 0;
	
	Soft_Timer_Enable |= 0x10;					
	FLOW_cnt = 0;
	Soft_Timer[4] = 16;
	EX0 = 1;
	f_no_water = 1;
	f_alarm_no_water = 0;
	LeftLiter = MAX_LITER-Liter;
	POT_Fail_Flag=0;
	ex_op_mode=Operation_Mode;
	Disp_Clean(ele);
	do_clean_flag=1; //20190319 jin
	if(ele ==0)   //Ken remove 30sec countdown, keep water flow ---
		LCM_NON_Disp_WaterFlow_count_down(ele,count_down_sec);  
//		LCM_NON_Disp_WaterFlow();
	PVoice = 16;						// Voice 電解槽洗淨中
	if(clean_after_trun_off)
	{
		Start_SleepCounter(5);
		count_down_sec=0;
		Water_In(1);
		LED_Disp (0x10);
	}
	else if(ele==0)
	{
		Start_SleepCounter(60);
		count_down_sec=0;
		Water_In(1);
		LED_Disp (0x10);
	}		
	else if((!Faucent_Version_Flag)&&(ele==1)&&(!Control_Selection))
	{
		Start_SleepCounter(60);
		count_down_sec=0;
		count_status=2;
		Water_In(1);
		//Acid_Polar(1);
		//SetCurrent(i1);
		SetCurrent(i1); //20190131 jin for acid clean big current
		Acid_Polar(1);  //20190131 jin for acid clean big current
		LED_Disp (0x10);
	}
	else if((!Control_Selection)&&(ele==1))
	{						
		while(key!=__KEY_START_CLEANING__)
		{
			key = ReceiveDate;
			
			if((!Soft_Timer[4])&&(vs_stop))
			{
				PlaySound(30);
				Soft_Timer[4] = 16*3;				
			}
			if(key==__KEY_DIN_SOUND__)
			{					
				ReceiveDate = 0;
				vs_stop=0;
				PlaySound(27);
			}
			else if(key==__KEY_DIN_SOUND_2__)
			{
				ReceiveDate = 0;
				vs_stop=1;
				Soft_Timer[4] = 16*3;
				PlaySound(30);				
			}
		}
		ReceiveDate=0;		
		start_up_mode=0;
		//Acid_Polar(1);
		//SetCurrent(i1);
		SetCurrent(i1); //20190131 jin for acid clean big current
		Acid_Polar(1);  //20190131 jin for acid clean big current
		Water_In(1);
		Start_SleepCounter(60);
		count_status=2;
		count_down_sec=0;
		LCM_NON_Disp_WaterFlow_count_down(ele,count_down_sec);
		//LCM_NON_Disp_WaterFlow();
		LED_Disp (0x10);
	}


	while (1)		
	{				
		Start_Hour_Count();	
		
		if (Soft_Timer[4]==0)	// 每秒更新流量一次
		{
			EX0 = 0;
			stop_sound_loop++;
			if((stop_sound_loop>3)&&(count_status==1))
			{
				stop_sound_loop=0;
				PlaySound(30);
			}
			if((ele==1)&&(count_status!=2))
			{
//		count_down_sec=0;			//Ken remove 30sec countdown						
				if((count_down_sec)&&(!count_status))
				{
//					count_down_sec=0;			//Ken remove 30sec countdown		
					if(!(Soft_Timer[6]))
					{
						if(RUN_LED==8)RUN_DER=1;
						else if(RUN_LED==1)	RUN_DER=0;
						if(!RUN_DER)
							RUN_LED++;
						else 
							RUN_LED--;
						LED_Disp(RUN_LED);
						Soft_Timer[6]=8;
					}
		//Ken			count_down_sec--;
					count_down_sec=0;			//Ken remove 30sec countdown		
					LCM_NON_Disp_WaterFlow_count_down(ele,count_down_sec);
//					LCM_NON_Disp_WaterFlow();
					if(!count_down_sec)
					{
						//Acid_Polar(1);//1=Acid
						//SetCurrent(i1);
						SetCurrent(i1); //20190131 jin for acid clean big current
		        Acid_Polar(1);  //20190131 jin for acid clean big current
						Water_In(1);//出水
						Start_SleepCounter(60);
						count_status=2;
						LED_Disp (0x10);
					}					
				}
				Soft_Timer[4] = 16;
				EX0 = 1;
			}
			else
			{
				if(demo_flag == 0)
					left_FlowCnt += FLOW_cnt;
				else
				{
					left_FlowCnt=48;
					FLOW_cnt = 10;
				}
				//LCM_Disp_WaterFlow(FLOW_cnt*100/128);///old water flow
				if(demo_flag == 0)
					clean_flow=FLOW_cnt*(25*2)/128;
				else
					clean_flow=18;
				//LCM_Disp_WaterFlow(clean_flow);//20
				SendMode(1, clean_flow);
			
				if (FLOW_cnt < 8)	   //org = 6
					f_no_water = 1;
				else
					f_no_water = 0;
				FLOW_cnt = 0;
				Soft_Timer[4] = 16;
				EX0 = 1;

				if (f_alarm_no_water)
				{
					Time_of_NoWater++;
					if (Time_of_NoWater==60)		// 無水1分鐘內未恢復供水    1 min = 60 sec
						break;				// 停止清洗跳出迴圈
				}

				if (Liter < MAX_LITER)
				{
					if (left_FlowCnt >= 1500)//previous setting 770 without discharge //3000 with discharge FM-02
					{
						Liter++;
						left_FlowCnt %= 1500;//previous setting 770 without discharge //3000 with discharge FM-02
					}
				}
				LeftLiter = MAX_LITER-Liter;			
			}
		}
		
		if(!count_down_sec)
		{
			INT = 1;
			if (ele==1)
				Volt_Ctrl();

			if (Is_SleepCounter_CountDown_Finish())		// 借用SleepCounter數60秒
			{
				Stop_SleepCounter();
				break;   		// exit from while
			}
			if (f_half_sec)		// 每半秒
			{
				if (! f_alarm_no_water)		// 畫水流圖一次
				{
					Disp_FlowGraph(rr++, PH_tab[8]);
					if (rr==4)	rr = 0;
				}
				else						
				{
					if (K2)
						CLR_K2();
					else	
						SET_K2();
					LCM_Disp_VOL_K2K3();	// 缺水水滴閃爍			
				}


				//====================================================================
				if (LeftLiter > 0)
					rr1 &= ~0x02;
				else
				{
					if (rr2)
						rr1 |= 0x02;
					else
						rr1 &= ~0x02;
				}
				if (! Filter_time_is_up())
					rr1 &= ~0x01;
				else	
				{
					if (rr2)
						rr1 |= 0x01;
					else
						rr1 &= ~0x01;
				}
				rr2 = ~rr2;
				LCM_Disp_KL(LeftLiter, rr1);

				if ((PowerOverHot_warning())||((!R03_R05N)&&((HOT_CHK)||(ads1115_temp>ADC1115_OVER_HOT)||(Operation_Mode==WARNING_MODE))))
				{
					if((!R03_R05N)&&((HOT_CHK)||(ads1115_temp>ADC1115_OVER_HOT)))	ErrCode = 3;
					Operation_Mode = WARNING_MODE;
					break;
				}
				//====================================================================
				f_half_sec = 0;
			}
			if (f_no_water)	// no water
			{
				if (! f_alarm_no_water)
				{
					if (! Check_WaterLackCounter_Start())
						Start_WaterLackCounter(10);
					else
					{
						if (Is_WaterLackCounter_CountDown_Finish())
						{
							Stop_WaterLackCounter();
							PlaySound(Language+15);		// voice 流量太小請調大
							R &= 0xC0;	// R1~R6 = 0
							LCM_Disp_PH_and_R(PH_tab[8]);
							f_alarm_no_water = 1;		// f_alarm_no_water is set after 10 sec from f_no_water
							LCM_Backlight(BL_Green);
						}
					}
				}
				else			// f_no_water && f_alarm_no_water
				{
					if (NoWaterVoice_Counter < 2)
					{
						if (! Check_WaterLackCounter_Start())
							Start_WaterLackCounter(5);
						else
						{
							if (Is_WaterLackCounter_CountDown_Finish())
							{
								Stop_WaterLackCounter();
								PlaySound(Language+15);		// voice 流量太小請調大
								NoWaterVoice_Counter++;
							}
						}
					}
				}
				if (ele==1)
				{
					i1 = 0;
					SetCurrent(0);	
				}			
			}
			else				// have water	
			{
				Disp_Clean(ele);
				Time_of_NoWater = 0;
				NoWaterVoice_Counter = 0;
				Stop_WaterLackCounter();
				if (f_alarm_no_water)		// no water -> have water
				{
					PVoice = 16;
					f_alarm_no_water = 0;
					CLR_K2();
					LCM_Disp_VOL_K2K3();
				}

				if (PVoice != 0)
				{
					if(clean_after_trun_off);
					else if(Control_Selection == 0 && Language != LAN_CHINESE)PlaySound(32);
					else
						PlaySound(Language+PVoice);
					PVoice = 0;
				}
				if (ele==1)
				{
					if (i1== 0)
					{
						SetCurrent(3000);
						i1 = 3000;
						ex_I=50;
						ex_electrolysis_type=WACID2;
					}	
				}			
			}
		}
	/*	else
		{
			if(Get_Key()==__KEY_ACTION__)
			{
				if(!count_status)
				{						
					LED_Disp (0x10);
					count_status=1;
					stop_sound_loop=0;
					PlaySound(30);
				}
				else
				{
					count_status=0;
					PlaySound(27);
				}
			}
		}*/

   }										// 按鍵釋放過再按__KEY_ACTION__才會結束迴圈
	Soft_Timer_Enable &= ~0x10;
	U8_switch_short(0);
	SetCurrent(0);
	Acid_Polar(0);
	EX0 = 0;
	after_action = 1;//play change filter sound on standby mode	
	R &= 0xC0;	// R1~R6 = 0
//	LCM_Disp_PH_and_R(PH_tab[electrolysis_type-1]);
		//Ken->
	if (P41==0) //
		LCM_Disp_PH_and_R(PH_tab0[electrolysis_type-1]);
	else
		LCM_Disp_PH_and_R(PH_tab[electrolysis_type-1]);
	//<-Ken
	FLOW_cnt = 0;
	LCM_Disp_WaterFlow(0);
	CLR_K2();
	LCM_Disp_VOL_K2K3();
	Stop_WaterLackCounter();
	Water_In(0);
	EEPROM_write_word(21, left_FlowCnt);
	EEPROM_write_word(23, Liter);
	if (Time_of_NoWater==60)
		Operation_Mode = SLEEP_MODE;
	else
	{
		if (Operation_Mode != WARNING_MODE)
		{
			Operation_Mode = STANDBY_MODE;
			 if(clean_after_trun_off);
			 else	
				{
					if(Control_Selection == 0 && Language != LAN_CHINESE)
						PlaySound(32);
					else
						PlaySound(Language+22);						// Voice 電解槽洗淨完畢
				}
		}
	}
	if (ele==1)
	{
		f_clean = 0;
		f_acid3 = 0;
		EEPROM_write_byte(81,F_NO_CLEAN);
	}
	else if(!clean_after_trun_off)
		f_acid3 = 0;
	clean_after_trun_off=0;	
	SendMode(0, Operation_Mode);	
}

void do_setup(void)
{
	unsigned char tmp, tmp1=1, vx3;
	unsigned char cExit=0,exit1=0,item=0,key=0, rr=0,no_count=0;
	unsigned int LeftLiter;
	ex_op_mode=Operation_Mode;
	INT = 0;
	Water_In(INT);
	PlaySound(33);
	CLR_SP12();
	vx3 = 0;
	if (X3)	vx3 = 1;
	CLR_X3();
	CLR_X4();
	CLR_ORP();
	CLR_L_MIN();
	CLR_ALKA();
	CLR_FILTER();
	LCM_Backlight(BL_Green);
	Clear_key_delay();
	key_delay[__KEY_MENU__-1] = 5;
	key_delay[__KEY_CLEAR__-1] = 5;
	key_delay[__KEY_RESET__-1] = 5;	
	key_delay[__KEY_PS_KEY__-1] = 5;	
	key_delay[__KEY_ACTION__-1] = 5;			
	while(! cExit)
	{
				Start_Hour_Count();
				if(item == SET_TDS)//TDS	
				{
					LCM_Backlight(BL_Green);
					LCM_Cls();
					CLR_L_MIN();					
					tmp1 = EEPROM_read_byte(30);
					LCM_Disp_TDS(tmp1,1);
					
					exit1 = 0;
					while(!exit1)
					{
						if (f_half_sec)
						{
							if (rr == 0)
								LCM_Disp_TDS(tmp1,1);
							else 
								LCM_Disp_TDS(tmp1,0);

							rr++;
							rr &= 0x01;
							no_count++;
							f_half_sec = 0;
						}
						key = Get_Key();
						
						if(key)Stop_SleepCounter();		// if any key is been pressed, stop sleep counter

						switch (key)
						{
							case __KEY_MENU__:
								cExit = 1;	// exit item 0
								exit1 = 1;
								break;		

							case __KEY_ENTER__:
								PlaySound(32);
								exit1 = 1;	// exit item 0
								item = SET_CURRENT;
								tmp=1;
								EEPROM_write_byte(30, tmp1);
								break;

							case __KEY_VOL_ADJ__:
						
								if (tmp1 > 0)
								{
									PlaySound(32);
									tmp1-=2;
									LCM_Disp_TDS(tmp1,1);
								}
								break;

							case __KEY_LOCK__:
								if (tmp1 < 24)
								{
									PlaySound(32);
									tmp1+=2;
									LCM_Disp_TDS(tmp1,1);
								}
								break;
							
							case __KEY_ALKA4__:
							case __KEY_ALKA3__:	
							case __KEY_ALKA2__:
							case __KEY_ALKA1__:
							case __KEY_ACID1__:	
							case __KEY_ACID2__:
							case __KEY_ACID3__:

							tmp = key - __KEY_ALKA4__; item = SET_CURRENT;exit1 = 1;break;

							case __KEY_PS_KEY__:
									demo_flag=1;
									PlaySound(33);
									break;
							case __KEY_NON__:
									if (! Check_SleepCounter_Start())
										Start_SleepCounter(60);		// 1 minutes, 60=1*60
									else
									{
											if (Is_SleepCounter_CountDown_Finish())
											{
												Stop_SleepCounter();
												cExit = 1;
												exit1 = 1;
												Operation_Mode = SLEEP_MODE;
											}
									}
									break;
							}

						}
					}
					if(item == SET_SEGMENT)
					{

						LCM_Cls();
						rr = 1;
						exit1 = 0;
						SET_SP12();
						if (tmp > 3)
						{
													
							CLR_ALKA();
							LCM_Disp_Alka(0);
							LCM_Disp_Acid(tmp-3);
							tmp1 = EEPROM_read_byte(31+tmp-1);
						}
						else
						{
							
							SET_ALKA();
							LCM_Disp_Alka(4-tmp);
							LCM_Disp_Acid(0);								
							tmp1 = EEPROM_read_byte(31+tmp);
						}
						Disp_electrolysis_type_withoutLmin(tmp+1);
						//LCM_Disp_PH_and_R(PH_tab[tmp]);
						//Ken->
						if (P41==0) //
							LCM_Disp_PH_and_R(PH_tab0[tmp]);
						else
							LCM_Disp_PH_and_R(PH_tab[tmp]);
						//<-Ken
						
						LCM_Disp_I_add(tmp1, 1);
						
				
						while (! exit1)
						{

							key = Get_Key();
							if(key)Stop_SleepCounter();		// if any key is been pressed, stop sleep counter
							switch (key)
							{
								case __KEY_MENU__:
										cExit = 1;	// exit item 0
										exit1 = 1;		// exit main loop	
										break;		

								case __KEY_ENTER__:
										PlaySound(32);
										exit1 = 1;	// exit item 0
										item = SET_LANGUAGE;
										break;

								case __KEY_ALKA4__:
								case __KEY_ALKA3__:	
								case __KEY_ALKA2__:
								case __KEY_ALKA1__:
								case __KEY_ACID1__:	
								case __KEY_ACID2__:
								case __KEY_ACID3__:
										tmp = key - __KEY_ALKA4__; item = SET_CURRENT;exit1 = 1;break;
																
								case __KEY_NON__:
									if (! Check_SleepCounter_Start())
										Start_SleepCounter(60);		// 1 minutes, 60=1*60
									else
									{
											if (Is_SleepCounter_CountDown_Finish())
											{
												Stop_SleepCounter();
												cExit = 1;
												exit1 = 1;
												Operation_Mode = SLEEP_MODE;
											}
									}
									break;
								}
							}

					}
					
					
					
				if(item == SET_CURRENT)
				{

						LCM_Cls();
						rr = 1;
						exit1 = 0;
						SET_SP12();
						if (tmp > 3)
						{
													
							CLR_ALKA();
							LCM_Disp_Alka(0);
							LCM_Disp_Acid(tmp-3);
							tmp1 = EEPROM_read_byte(31+tmp-1);
						}
						else
						{
							
							SET_ALKA();
							LCM_Disp_Alka(4-tmp);
							LCM_Disp_Acid(0);
							tmp1 = EEPROM_read_byte(31+tmp);
						}
						Disp_electrolysis_type_withoutLmin(tmp+1);
						//LCM_Disp_PH_and_R(PH_tab[tmp]);
						//Ken->
						if (P41==0)
							LCM_Disp_PH_and_R(PH_tab0[tmp]);
						else
							LCM_Disp_PH_and_R(PH_tab[tmp]);
						//<-Ken	
						LCM_Disp_I_add(tmp1, 1);
						
				
						while (! exit1)
						{

							key = Get_Key();
							if(key)Stop_SleepCounter();		// if any key is been pressed, stop sleep counter
							if (f_half_sec)
							{
								if (rr == 0)
								{
									SET_SP12();
									LCM_Disp_I_add(tmp1, 1);
								}
								else 
								{
									CLR_SP12();
									LCM_Disp_I_add(tmp1, 0);	// Blank
								}

								rr++;
								rr &= 0x01;
								f_half_sec = 0;
							}
							
	
							switch (key)
							{
								case __KEY_MENU__:
										cExit = 1;	// exit item 0
										exit1 = 1;		// exit main loop	
										break;		

								case __KEY_ENTER__:
										PlaySound(32);
										exit1 = 1;	// exit item 0
										item = SET_SEGMENT;
										if(tmp<4)
											EEPROM_write_byte(31+tmp, tmp1);
										else
											EEPROM_write_byte(31+tmp-1, tmp1);
										I_add[tmp] = tmp1;
										break;

								case __KEY_VOL_ADJ__:
										if (tmp1 > 0)
										{
											PlaySound(32);
											tmp1--;
											LCM_Disp_I_add(tmp1, 1);
										}		
										break;

								case __KEY_LOCK__:
									if (tmp1 < 10)	 	  // JOHNSON
									{
										PlaySound(32);
										tmp1++;
										LCM_Disp_I_add(tmp1, 1);
									}
									break;
																
								case __KEY_NON__:
									if (! Check_SleepCounter_Start())
										Start_SleepCounter(60);		// 1 minutes, 60=1*60
									else
									{
											if (Is_SleepCounter_CountDown_Finish())
											{
												Stop_SleepCounter();
												cExit = 1;
												exit1 = 1;
												Operation_Mode = SLEEP_MODE;
											}
									}
									break;
								}
							}

					}
					if (item == SET_LANGUAGE)
					{
							LCM_Backlight(BL_Green);
							LCM_Cls();
							CLR_ALKA();
							LCM_Disp_Alka(0);
							LCM_Disp_Acid(0);
							CLR_L_MIN();
							CLR_SP12();
							tmp = Language/32;
							LCM_Disp_PH_and_R(LANGUAGE_STRING[tmp]);
							exit1 = 0;
							while (! exit1)
							{
								key = Get_Key();
								if(key)Stop_SleepCounter();		// if any key is been pressed, stop sleep counter
								if (f_half_sec)
								{
										if (rr == 0)
												LCM_Disp_PH_and_R(LANGUAGE_STRING[tmp]);
										else 
												LCM_Disp_PH_and_R(PH_tab[9]);		// Blank
										rr++;
										rr &= 0x01;
										f_half_sec = 0;
								}

								switch (key)
								{
									case __KEY_MENU__:
											cExit = 1;	// exit item 0
											exit1 = 1;		// exit main loop	
											break;		

									case __KEY_ENTER__:
											PlaySound(32);
											exit1 = 1;	// exit item 0
											item = SET_WASH;
											EEPROM_write_byte(20, tmp);
											if(!tmp) EEPROM_write_byte(38, LAN_CHINESE);
											else	EEPROM_write_byte(38, Area_Eng);
											Language = tmp*32;
											break;

									case __KEY_VOL_ADJ__:
											PlaySound(32);
											if(tmp==0)
												tmp=5;
											else if(tmp==2)
												tmp=0;
											else 
												tmp--;
											LCM_Disp_PH_and_R(LANGUAGE_STRING[tmp]);
											break;
									case __KEY_LOCK__:
											PlaySound(32);
											if(tmp==5)
												tmp=0;
											else if(tmp==0)
												tmp=2;
											else 
												tmp++;
											LCM_Disp_PH_and_R(LANGUAGE_STRING[tmp]);
											break;
					
									case __KEY_ALKA4__:
									case __KEY_ALKA3__:	
									case __KEY_ALKA2__:
									case __KEY_ALKA1__:
									case __KEY_ACID1__:	
									case __KEY_ACID2__:
									case __KEY_ACID3__:
											tmp = key - __KEY_ALKA4__; item = SET_CURRENT;exit1 = 1;break;
									
									case __KEY_NON__:
											if (! Check_SleepCounter_Start())
													Start_SleepCounter(60);		// 1 minutes, 60=1*60
											else
											{
													if (Is_SleepCounter_CountDown_Finish())
													{
															Stop_SleepCounter();
															cExit = 1;
															exit1 = 1;
															Operation_Mode = SLEEP_MODE;
													}
											}
											break;
									}				
							}
						}
						
			if (item == SET_WASH)
			{
				LCM_Backlight(BL_Green);
				tmp = EEPROM_read_byte(19);
				LCM_Cls();
				LCM_Disp_PH_and_R(PH_tab[8]);
				Filter_time_is_up();
				LeftLiter = MAX_LITER-Liter;
				LCM_Disp_KL(LeftLiter, 0);
				LCM_Disp_WashLiter(tab_wash[tmp], 1);				
				exit1 = 0;
				rr = 0;
				while (! exit1)
				{
					key = Get_Key();
					
					if(key)Stop_SleepCounter();		// if any key is been pressed, stop sleep counter
					if (f_half_sec)
					{
						if (rr)
							LCM_Disp_WashLiter(tab_wash[tmp], 1);
						else
							LCM_Disp_WashLiter(tab_wash[tmp], 0);
						rr++;
						rr &= 0x01;
						f_half_sec = 0;
					}

					switch (key)
					{
						case __KEY_MENU__:
							cExit = 1;	// exit item 0
							exit1 = 1;
							break;			

						case __KEY_ENTER__:
							PlaySound(32);
							exit1 = 1;	// exit item 0
							item = SET_TDS;
							EEPROM_write_byte(19, tmp);
							WashLiter = tab_wash[tmp];
							break;

						case __KEY_VOL_ADJ__:
							PlaySound(32);
							if (tmp == 0)
								tmp = 5;	
							else
								tmp--;		
							LCM_Disp_WashLiter(tab_wash[tmp], 1);
							break;

						case __KEY_LOCK__:
							PlaySound(32);							
							if (tmp == 5)
								tmp = 0;
							else 
								tmp++;
							LCM_Disp_WashLiter(tab_wash[tmp], 1);
							break;
							
						case __KEY_ALKA4__:
						case __KEY_ALKA3__:	
						case __KEY_ALKA2__:
						case __KEY_ALKA1__:
						case __KEY_ACID1__:	
						case __KEY_ACID2__:
						case __KEY_ACID3__:
								tmp = key - __KEY_ALKA4__; item = SET_CURRENT;exit1 = 1;break;
					
						case __KEY_CLEAR__:
								PlaySound(32);
								LeftLiter = 9000;
								SET_K5();
								SET_K6();		
								SET_K7();
								SET_K8();		
								SET_K9();		
								LCM_Disp_KL(LeftLiter, 0);
								left_FlowCnt = 0;		
								Liter = 0;			
								UseMin = 0;			
								UseHour = 0;						
								EEPROM_write_word(21, left_FlowCnt);	// left_FlowCnt on EEPROM address 21~22
								delay(300);
								EEPROM_write_word(23, Liter);	// Liter on EEPROM address 23~24
								delay(300);
								EEPROM_write_word(25, UseMin);	// filter used sec 25~26
								delay(300);
								EEPROM_write_word(27, UseHour);	// filter used hour 27~28			
								break;
						
						case __KEY_RESET__:
								ReceiveDate = 0;
								Operation_Mode = RESET_MODE;
								cExit = 1;
								exit1 = 1;
								break;
						case __KEY_ACTION__:								
								clean_w_PJ6000();
								PlaySound(32);
								cExit = 1;	// exit item 0
								exit1 = 1;
								Operation_Mode = SLEEP_MODE;
								break;
													
						case __KEY_NON__:
									if (! Check_SleepCounter_Start())
										Start_SleepCounter(60);		// 1 minutes, 60=1*60
									else
									{
											if (Is_SleepCounter_CountDown_Finish())
											{
												Stop_SleepCounter();
												cExit = 1;
												exit1 = 1;
												Operation_Mode = SLEEP_MODE;
											}
									}
									break;
						}				
					}
				} 
      }
	
	PlaySound(33);
		if (vx3)
			SET_X3();
		if((Operation_Mode != SLEEP_MODE) && (Operation_Mode != RESET_MODE))
			Operation_Mode = STANDBY_MODE;	
	
}

void do_sleep(void)
{
     unsigned char stty =0;
  
  	ex_op_mode=Operation_Mode;
	INT = 0;
	Water_In(INT);
	POT_Fail_Flag=0;	
	if(Control_Selection != 0)
	{
			LCM_Backlight(BL_NoColor);
			LCM_Cls();
			LED_Disp(0x00);
			Clear_key_delay();

		    	 
			while ((!stty)&&(!Get_Key()))
			    {
		     	  if(PowerOverHot_warning())		 //ADD	 for pcb hot in the sleep mode
	                {
					  stty = 1;
	    	          LCM_Backlight(BL_Red);
 		              Operation_Mode = WARNING_MODE;
	                }
				      Start_Hour_Count();
			          ADS1115_Check(0);
			    }
	      }
	      if(PowerOverHot_warning())		 //ADD	 for pcb hot in the sleep mode
	       {
			 LCM_Backlight(BL_Red);
 		     Operation_Mode = WARNING_MODE;
		   }
	if(f_lock==0)electrolysis_type = WALKA3;
		ReceiveDate = 0;
		Operation_Mode = STANDBY_MODE;
}


unsigned char CurrentSeg(unsigned int value)
{
	unsigned char retc;
	unsigned int i;
	
	i =	value/3;//20140417 change the flow to total folow

	if (i < 12)
		retc = 0;
	else if (i < 15)
		retc = 1;
	else if (i < 18)
		retc = 2;
	else if (i < 22)
		retc = 3;
	else if (i < 25)
		retc = 4;
	else if (i < 28)
		retc = 5;
	else
		retc = 6;

	return retc;
}
void do_electrolyze(void)
{
	int set_i;
	unsigned char key=0,change_flage=0,chage_loop=0;					// key1為了記錄按鍵是否被釋放過
	unsigned char rr=0, rr1=0,first=1;

	unsigned int F0;
	unsigned int Time_of_NoOperation=0;
	unsigned char Time_of_NoWater=0;
	unsigned char NoWaterVoice_Counter=0;
	unsigned char seg0=0, seg1;
	unsigned char i=0,action_sound=0,acid_loop=0,decrement_rstep,over_current_loop=0,high_current_loop=0;
	static unsigned char alk_flag=0;
	static unsigned int ex_time=0;
	bit rr2=0;
	flow_cnt_tmp[0]=0;
	flow_cnt_tmp[1]=0;
	INT=1;
	EEPROM_ELECTROLYSIS_Read();
	Water_In(1);
	F0 = 0;
	f_half_sec = 0;
	Soft_Timer_Enable |= 0x10;					
	f_no_water = 1;
	f_alarm_no_water = 0;
	LeftLiter = MAX_LITER-Liter;
	POT_Fail_Flag=0;
	ReceiveDate = 0;
	PVoice = tab_voice[electrolysis_type-1];
	Clear_key_delay();	
	ex_op_mode=Operation_Mode;
	key_delay[__KEY_LOCK__-1] = 3;
	if (electrolysis_type > 5)
	{
		Acid_Polar(1);
		aci_flag=1;
		alk_flag=0;
	}
	else
	{
		aci_flag=0;
		alk_flag=1;
	}
	if(ex_time!=(UseMin+UseHour))
	{	
		ex_alk_flag=alk_flag;
		ex_time=(UseMin+UseHour);
		ex_I=I_add[electrolysis_type-1];
	}
	
	
	/*
	if(first==1 && Language==LAN_CZECH*32) //20190418 jin
					{
						Voice_Timer_2=16+8; //1sec
						Voice_timer_flag_2=1;
						first=0;
						PVoice_tmp=0; //stop voice
					}
		*/
	
	EX0 = 0;
	FLOW_cnt = 0;
	Soft_Timer[4] = 16;
	EX0 = 1;

	if (electrolysis_type == WACID3)
			f_acid3 = 1;
	else
			f_acid3 = 0;
	if(Control_Selection == 0)
			key = ReceiveDate;
	else 
			key = Get_Key();
	while (key != __KEY_ACTION__)		// 按鍵未釋放過或未偵測到按下__KEY_ACTION__
	{
		INT=1;
		
		Start_Hour_Count();
		if ((LeftLiter < 900)||(UseHour > 7884))//
		{
			change_flage=1;
			if(change_flage==1 && chage_loop == 0)
			{
				SendMode(0, Operation_Mode);
				chage_loop++;
			}
			else if(chage_loop > 20)
				chage_loop = 0;
		}
		Volt_Ctrl();
		if(Control_Selection == 0) 
			key = ReceiveDate;
		else
		{
			key = Get_Key();
			if(key != 0)
				Time_of_NoOperation = 0;   //jj
		}					



		if ((electrolysis_type !=WACID3) && (electrolysis_type != (key-1)))
		{
			switch (key)
			{
				case __KEY_ALKA4__:
					ReceiveDate = 0;
					if(ReceiveDate != electrolysis_type)
						Time_of_NoOperation = 0;		//jj			
					if (! f_lock)
					{
						Acid_Polar(0);
						acid_loop=0;
						alk_flag=1;		
						high_current_loop=0;		
						electrolysis_type = WALKA4;
						Disp_electrolysis_type(electrolysis_type);
						PVoice = tab_voice[electrolysis_type-1];
						seg0 = 7;		
						if(ex_electrolysis_type>5)
							ex_alk_flag=0;
						else
							ex_alk_flag=1;		
					}
					action_sound=1;
					break;
					
				case __KEY_ALKA3__:
					ReceiveDate = 0;
					if(ReceiveDate != electrolysis_type)
						Time_of_NoOperation = 0;	//jj				
					if (! f_lock)
					{
						acid_loop=0;
						alk_flag=1;
						Acid_Polar(0);
						high_current_loop=0;
						electrolysis_type = WALKA3;
						Disp_electrolysis_type(electrolysis_type);
						PVoice = tab_voice[electrolysis_type-1];
						seg0 = 8;
						if(ex_electrolysis_type>5)
							ex_alk_flag=0;
						else
							ex_alk_flag=1;								
					}
					action_sound=1;
					break;	

				case __KEY_ALKA2__:
					ReceiveDate = 0;
					if(ReceiveDate != electrolysis_type)
						Time_of_NoOperation = 0;  //					
					if (! f_lock)
					{
						acid_loop=0;
						alk_flag=1;
						Acid_Polar(0);
						high_current_loop=0;
						electrolysis_type = WALKA2;
						Disp_electrolysis_type(electrolysis_type);
						PVoice = tab_voice[electrolysis_type-1];
						seg0 = 9;
						if(ex_electrolysis_type>5)
							ex_alk_flag=0;
						else
							ex_alk_flag=1;								
					}
					action_sound=1;
					break;
	
				case __KEY_ALKA1__:
					ReceiveDate = 0;
					if(ReceiveDate != electrolysis_type)
						Time_of_NoOperation = 0;	//jj				
					if (! f_lock)
					{
						acid_loop=0;
						alk_flag=1;
						Acid_Polar(0);
						high_current_loop=0;
						electrolysis_type = WALKA1;
						Disp_electrolysis_type(electrolysis_type);
						PVoice = tab_voice[electrolysis_type-1];
						seg0 = 10;
						if(ex_electrolysis_type>5)
							ex_alk_flag=0;
						else
							ex_alk_flag=1;								
					}
					action_sound=1;
					break;		

				case __KEY_FILTER__:
					ReceiveDate = 0;
					if(ReceiveDate != electrolysis_type)
						Time_of_NoOperation = 0;	//				
					if (! f_lock)
					{
						acid_loop=0;
						alk_flag=1;
						Acid_Polar(0);
						high_current_loop=0;
						electrolysis_type = WFILTER;
						Disp_electrolysis_type(electrolysis_type);
						PVoice = tab_voice[electrolysis_type-1];
						seg0 = 11;
						if(ex_electrolysis_type>5)
							ex_alk_flag=0;
						else
							ex_alk_flag=1;								
					}
					action_sound=1;
					break;

				case __KEY_ACID1__:
					ReceiveDate = 0;
					if(ReceiveDate != electrolysis_type)
						Time_of_NoOperation = 0; //	 jj				
					if (! f_lock)
					{
						acid_loop=0;
						alk_flag=0;
						Acid_Polar(1);
						high_current_loop=0;
						electrolysis_type = WACID1;
						Disp_electrolysis_type(electrolysis_type);
						PVoice = tab_voice[electrolysis_type-1];
						seg0 = 12;
						if(ex_electrolysis_type>5)
							ex_alk_flag=0;
						else
							ex_alk_flag=1;								
					}
					action_sound=1;
					break;

				case __KEY_ACID2__:
					ReceiveDate = 0;
					if(ReceiveDate != electrolysis_type)
						Time_of_NoOperation = 0;	//	jj			
					if (! f_lock)
					{
						acid_loop=0;
						alk_flag=0;
						high_current_loop=0;
						Acid_Polar(1);
						electrolysis_type = WACID2;
						Disp_electrolysis_type(electrolysis_type);
						PVoice = tab_voice[electrolysis_type-1];
						seg0 = 13;
						if(ex_electrolysis_type>5)
							ex_alk_flag=0;
						else
							ex_alk_flag=1;								
					}
					action_sound=1;
					break;
					
				case __KEY_DIN_SOUND__:		
					ReceiveDate = 0;
					PlaySound(32);
					break;	
			}
		}
		if(key == __KEY_ACTION__)
		{
			if(f_clean)
				LED_Disp(electrolysis_type | 0x10);
			else
				LED_Disp(electrolysis_type);
			Set_U7_10k();
			SetCurrent(0);
			Acid_Polar(0);
			if((electrolysis_type!=WALKA4)&&(electrolysis_type!=WACID2))Water_In(0);			
			break;
		}
		ex_electrolysis_type=electrolysis_type;
		switch (key)
		{
			case __KEY_LOCK__:
				ReceiveDate = 0;
				f_lock = ~f_lock;
				if (f_lock)
				{
					EEPROM_write_byte(18, 0x80+electrolysis_type); 
					SET_K3();
					PlaySound(Language+8);
				}
				else
				{
					EEPROM_write_byte(18, 0); 
					CLR_K3();
					PlaySound(Language+9);
				}
				LCM_Disp_VOL_K2K3();	// LOCK 圖示		
				break;

			case __KEY_MENU__:
				ReceiveDate = 0;
				PlaySound(32);
				if (! f_show_orp)
				{
					f_show_orp = 1;
					SET_MV();
					SET_ORP();

				}
				else
				{
					f_show_orp = 0;
					CLR_MV();
					CLR_ORP();
				}
				break;

				
			case __KEY_VOL_ADJ__:
				ReceiveDate = 0;
				Volume++;
				Volume &= 0x03;
				PlaySound(0x30+Volume);
				if (Volume==0)
				{
					CLR_X2();		CLR_X3();		CLR_X4();
				}
				if (Volume==1)
				{
					SET_X2();		CLR_X3();		CLR_X4();
				}
				if (Volume==2)
				{
					SET_X2();		SET_X3();		CLR_X4();
				}
				if (Volume==3)
				{
					SET_X2();		SET_X3();		SET_X4();
				}
				LCM_Disp_VOL_K2K3();
				PlaySound(32);
				break;
		}
		SendMode(0, Operation_Mode);
		if (f_half_sec)		// 每半秒
		{
			//Disp_FlowGraph(rr, PH_tab[electrolysis_type-1]);
			//Ken->
			if (P41==0) //
				Disp_FlowGraph(rr, PH_tab0[electrolysis_type-1]);
			else
				Disp_FlowGraph(rr, PH_tab[electrolysis_type-1]);
			//<-Ken			
			
			if (! f_alarm_no_water)		// 畫水流圖一次
			{	
				if(electrolysis_type == WFILTER)//20140307 Filterd show static elec icon
					rr=3;
				else
				{
					if (rr==4)
						rr = 0;
					else
						rr++;
				}
			}
			else
			{
				rr = 4;
				if (K2)
					CLR_K2();
				else	
					SET_K2();
				LCM_Disp_VOL_K2K3();	// 缺水水滴閃爍			
			}


			//====================================================================
			if (LeftLiter > 0)
				rr1 &= ~0x02;
			else							// 過水已達9000L
			{
				if (rr2)
					rr1 |= 0x02;
				else      
					rr1 &= ~0x02;
			}
			if (! Filter_time_is_up())
				rr1 &= ~0x01;
			else							// 過水已達365天
			{
				if (rr2)
					rr1 |= 0x01;
				else
					rr1 &= ~0x01;
			}
			rr2 = ~rr2;
			LCM_Disp_KL(LeftLiter, rr1);		// 顯示濾芯壽命

			if ((PowerOverHot_warning())||((!R03_R05N)&&((HOT_CHK)||(ads1115_temp>ADC1115_OVER_HOT)||(Operation_Mode==WARNING_MODE))))
			{
				if((!R03_R05N)&&((HOT_CHK)||(ads1115_temp>ADC1115_OVER_HOT)))	ErrCode = 3;
				Operation_Mode = WARNING_MODE;
				key = __KEY_ACTION__;
			}
//			sencahr(Operation_Mode);//UART Rx[0]
			//====================================================================

			f_half_sec = 0;
		}
	
		if (Soft_Timer[4]==0)	// 每秒更新流量一次
		{
			EX0 = 0;
			if(demo_flag != 0)
				flow_cnt_tmp[1]=48;
			else
				flow_cnt_tmp[1] = FLOW_cnt;
			FLOW_cnt = 0;
			Soft_Timer[4] = 16;
			EX0 = 1;
			if(ADS1110_flag)
			{
				i++;
				if((i>3)&&(ADS1110_read_voltage(1)>(set_i*1.05)))
				{	
					i=0;
					Decrement_R(10);
				}
			}			
				
			left_FlowCnt += flow_cnt_tmp[1];                ///repair
			//F0 = (flow_cnt_tmp[1]+flow_cnt_tmp[0])*50/128;//old water flow
			//F0 = (flow_cnt_tmp[1]+flow_cnt_tmp[0])*16/128;//10 FM - 02
			F0 = (flow_cnt_tmp[1]+flow_cnt_tmp[0])*25/128;//FM-03

			LCM_Disp_WaterFlow(F0);
			//delay(200);
			SendMode(1, F0);
	

			if (! f_no_water)
			{
					seg1 = CurrentSeg(F0);
					if (seg1 != seg0)
					{				
						//set_i = tab_i[electrolysis_type-1][seg1]+(40-I_add_t-I_add[electrolysis_type-1])*i_multiply[electrolysis_type-1];	//with water flow
						//set_i = tab_i_fix[electrolysis_type-1]+(40-I_add_t-I_add[electrolysis_type-1])*i_multiply[electrolysis_type-1];	//without water flow
						set_i = I_add[electrolysis_type-1] * 100;
						if (set_i < 0)
							set_i = 0;
						if (set_i >= 5000)
							set_i = 6000;
						//if (electrolysis_type == WACID3)
						//	set_i = 5000;//20131206 fix max current on ACID3
						SetCurrent(set_i);
						seg0 = seg1; 
					}
					if(!ADS1110_flag)
					{
							if(ex_alk_flag!=alk_flag)
							{	
								
								if(((ex_I - I_add[electrolysis_type-1])>=25)||((I_add[electrolysis_type-1]-ex_I)>=25))
									decrement_rstep=10;
								else if(((ex_I - I_add[electrolysis_type-1])>=15)||((I_add[electrolysis_type-1]-ex_I)>=15))
									decrement_rstep=5;
								else
									decrement_rstep=1;
								
								if(decrement_rstep!=1)
								{	
									acid_loop++;
									if(acid_loop<10)SetCurrent(0);
									else if(acid_loop<12)SetCurrent(set_i);
									else if(acid_loop<18);
									else if(acid_loop<19)Decrement_R(decrement_rstep*2);
									else if(acid_loop<22);
									else if(acid_loop<23)Decrement_R(decrement_rstep);
									else if(acid_loop<25);
									else if(acid_loop<26)Decrement_R(decrement_rstep);
									else if(acid_loop<30);
									else if(acid_loop<31)
										Decrement_R(decrement_rstep*2);
									else if(acid_loop<32);
									else if(acid_loop<33)
										Decrement_R(decrement_rstep);
									else if(acid_loop<34);
									else if(acid_loop<35)
										Decrement_R(decrement_rstep);
									else if(acid_loop<36);
									else if(acid_loop<37)
										Decrement_R(decrement_rstep);
									else if(acid_loop<39);
									else if(acid_loop<40)Decrement_R(decrement_rstep);
									else if(acid_loop<42);
									else
									{
										ex_alk_flag=alk_flag;
										ex_I=I_add[electrolysis_type-1];
										over_current_loop=0;							
										acid_loop=0;
									}									

								}
								else if(decrement_rstep==1)
								{
									acid_loop++;
									if(acid_loop<3);
									else if(acid_loop<5)SetCurrent(set_i);
									else if(acid_loop<8);
									else if(acid_loop<14)Decrement_R(decrement_rstep*15);
									else if(acid_loop<16);
									else if(acid_loop<30)Decrement_R(decrement_rstep*2);
									else if(acid_loop<33);
									else if(acid_loop<37)Decrement_R(decrement_rstep*2);			
									else 
									{
										ex_alk_flag=alk_flag;
										ex_I=I_add[electrolysis_type-1];
										over_current_loop=0;							
										acid_loop=0;
									}										
								}
								else
								{								
									ex_alk_flag=alk_flag;
									ex_I=I_add[electrolysis_type-1];
									over_current_loop=0;							
									acid_loop=0;
									ex_electrolysis_type=electrolysis_type;
								}
							}
							else if((ex_I >=45)&&(ex_I!=I_add[electrolysis_type-1])&&((ex_I-I_add[electrolysis_type-1])<=15))
							{
								over_current_loop++;
								if(over_current_loop < 3);
								else if(over_current_loop < 5)Decrement_R(10);
								else if(over_current_loop < 8);
								else if(over_current_loop < 14)Decrement_R(10);
								else if(over_current_loop < 16);
								else if(over_current_loop < 25)Decrement_R(2);
								else if(over_current_loop < 30);
								else if(over_current_loop < 34)Decrement_R(1);	
								else
								{
									ex_I=I_add[electrolysis_type-1];
									over_current_loop=0;
									ex_electrolysis_type=electrolysis_type;
								}	
							}
							else if((high_current_loop<40)&&(I_add[electrolysis_type-1]>=35)&&(I_add[electrolysis_type-1]<46))
							{
								if(high_current_loop < 8);
								else if(high_current_loop < 14)Decrement_R(10);
								else if(high_current_loop < 16);
								else if(high_current_loop < 25)Decrement_R(2);						
								else
								{
									ex_I=I_add[electrolysis_type-1];
									ex_electrolysis_type=electrolysis_type;
								}	
								
							}
							else
							{
								ex_I=I_add[electrolysis_type-1];
								acid_loop=0;
								ex_electrolysis_type=electrolysis_type;
							}
							high_current_loop++;
					}
			}
			else	
			{
				Set_U7_10k();
				SetCurrent(0);	
				seg0 = 7;
			}
			
			if (flow_cnt_tmp[1] < 7)//6 ==>0,  L/min 20==> 0.2L/min //14 ---> after change the flow
				f_no_water = 1;
			else
				f_no_water = 0;
			flow_cnt_tmp[0] = flow_cnt_tmp[1];

			Time_of_NoOperation++;		    //johnson
			if (Time_of_NoOperation == 900)	// 出水15分鐘內不操作      15 min = 900 sec
				key = __KEY_ACTION__;		// 停止電解跳出迴圈

			if (f_alarm_no_water)
			{
				Time_of_NoWater++;
				if (Time_of_NoWater==60)	// 無水1分鐘內未恢復供水    1 min = 60 sec
					key = __KEY_ACTION__;	// 停止電解跳出迴圈
			}
	
			if (Liter < MAX_LITER) //20170121 for 902? if ((Liter < MAX_LITER)&&(!demo_flag))
			{
				if (left_FlowCnt >= 1500)//previous setting 770 without discharge //3000 with discharge FM-02
				{							 ///repair
					Liter++;
					if (Liter%WashLiter==0) 
					{
						if (UseHour < MAX_HOUR)//20170326 jin
						    f_clean = 1;
						    EEPROM_write_byte(81, F_CLEAN);
					}
					left_FlowCnt %= 1500;//previous setting 770 without discharge //3000 with discharge FM-02
				}
			}
			LeftLiter = MAX_LITER-Liter;

			

		}

		if (f_no_water)	// no water
		{
		
			if (! f_alarm_no_water)
			{
				if (! Check_WaterLackCounter_Start())
					Start_WaterLackCounter(10);
				else
				{
					if (Is_WaterLackCounter_CountDown_Finish())
					{
						Stop_WaterLackCounter();
						f_alarm_no_water = 1;		// f_alarm_no_water is set after 10 sec from f_no_water
						PlaySound(Language+15);		// voice 流量太小請調
						LCM_Backlight(BL_Green);
					}
				}
			}
			else
			{
				if (NoWaterVoice_Counter < 2)
				{
					if (! Check_WaterLackCounter_Start())
						Start_WaterLackCounter(5);
					else
					{
						if (Is_WaterLackCounter_CountDown_Finish())
						{
							Stop_WaterLackCounter();
							PlaySound(Language+15);		// voice 流量太小請調
							NoWaterVoice_Counter++;
						}
					}
				}

			}
		}
		else				// have water	
		{
			Time_of_NoWater=0;
			NoWaterVoice_Counter = 0;
			Stop_WaterLackCounter();
			if (f_alarm_no_water)		// no water -> have water
			{
				f_alarm_no_water = 0;
				CLR_K2();
				LCM_Disp_VOL_K2K3();
				PVoice = tab_voice[electrolysis_type-1];
				LCM_Backlight(electrolysis_type);
			}

			if (PVoice != 0)
			{
				if((aci_flag == 1) && (electrolysis_type < 6))
				{
					aci_flag = 0;
					delay(4000);//delay 1S
					FLOW_cnt = 0;//prevent an excess of transient flow when switch level
					delay(4000);//delay 1S					
				}
				else if((electrolysis_type > 5))
					aci_flag = 1;
				else 
					aci_flag = 0;
				
				if(Control_Selection == 0 && Language != LAN_CHINESE)
				{
					if(action_sound == 1)
						PlaySound(34);				
				}
				else
				{
									
					delay(3000);
					PlaySound(Language+PVoice);	
											
				}	
				if(Usage_Counter[electrolysis_type-1]>=4294967295 )
				{
						overflow_flag|=Over_each_flag[(electrolysis_type-1)];
						Usage_Counter[electrolysis_type-1]=0;
				}	
				Usage_Counter[electrolysis_type-1]++;
				PVoice = 0;						
			}			
		}
		
		delay(50);
		delay(50);
	}
	ReceiveDate=0;
	Soft_Timer_Enable &= ~0x10;
	EX0 = 0;
	EEPROM_write_word(21, left_FlowCnt);
	EEPROM_write_word(23, Liter);	
	EEPROM_write_hide_info();
	after_action = 1;
	if ((Time_of_NoOperation==900) || (Time_of_NoWater==60))
	{
		after_action = 0;//20190325 jin
		Operation_Mode = SLEEP_MODE;
	}
	else
	{
		if (Operation_Mode != WARNING_MODE)
		{
			if(f_clean == 1)
			{
				clean_after_trun_off=0;
				Operation_Mode = CLEAN_MODE;
			}
			else if(electrolysis_type!=WFILTER)
			{
				if((!Control_Selection)&&(!Choose_Default))
				{
					clean_after_trun_off=1;
					Operation_Mode = CLEAN_MODE;
				}
				else if((Control_Selection)&&((electrolysis_type==WALKA4)||(electrolysis_type==WACID2)))
				{
					clean_after_trun_off=0;
					Operation_Mode = STANDBY_MODE;
					Soft_Timer[5]=5;
					//while(Soft_Timer[5])
					{	
						Set_U7_10k();
						SetCurrent(0);
						Acid_Polar(0);	
					}						
				}
				else
				{
					clean_after_trun_off=0;
					Operation_Mode = STANDBY_MODE;	
				}					
			}
			else
			{
				clean_after_trun_off=0;
				Operation_Mode = STANDBY_MODE;	
			}
			if(Control_Selection == 0 && Language != LAN_CHINESE)
				PlaySound(35);
			else
				PlaySound(Language+25);		// Voice -- turn off
		}		
	}
	PVoice = 0;
	U8_switch_short(0);
	INT=0;
	R &= 0xC0;	// R1~R6 = 0
	//LCM_Disp_PH_and_R(PH_tab[electrolysis_type-1]);
	//Ken->
	if (P41==0) //
		LCM_Disp_PH_and_R(PH_tab0[electrolysis_type-1]);
	else
		LCM_Disp_PH_and_R(PH_tab[electrolysis_type-1]);
	//<-Ken
	FLOW_cnt = 0;
	LCM_Disp_WaterFlow(0);
	CLR_K2();
	LCM_Disp_VOL_K2K3();
	Set_U7_10k();
	SetCurrent(0);
	Acid_Polar(0);
	if(!Soft_Timer[5])Water_In(0);
	ex_alk_flag=alk_flag;
	ex_I=I_add[electrolysis_type-1];
	over_current_loop=0;	
	acid_loop=0;
	f_show_orp = 0;
	seg0 = 13;
	Stop_WaterLackCounter();
	ex_time=(UseMin+UseHour);	
}

void PORT_Init(void)
{
	LCM_DataLE = 0;
	LED_DataLE = 0;
	IO_DataLE = 0;
	IO_DataOE	= 1;
 	P0M1 = 0xFF;
	P1M0 = 0x03;
	P1M1 = 0x38;   // define P1.3 P1.4 P1.5 as push-pull, P1.1 as internal only
	P2M1 = 0xC7; 
	P3M1 = 0xFA;//Set UART port(P3.1),INT#(P3.3) to push-pull
 	P3M0 = 0x05;
	//P3 = 0;
	P0 = 0;	
	IO_DataLE = 1;
	_nop_();
	_nop_();
   	_nop_();
	IO_DataLE=0;
	IO_DataOE = 0;  
	V_DETECT = 1;  
	R03_R05N=HOT_CHK;
}

void do_warning(void) //ER1: Power Board Over Hot, ER2: Water Over Hot, ER3: Board Over Hot, ER4: POT Fail
{
	unsigned char voice,voice_count=0;
	unsigned int t0;
	ex_op_mode=Operation_Mode;
	INT = 0;
	LCM_Cls();
	LCM_Backlight(BL_Red);
	Water_In(INT);
	//t0 = PowerOverHot_warning();
	if(ErrCode == 4)
	{
		//ErrCode = 4;
		voice = 36;
		LCM_Disp_Error_Code(ErrCode);
	}
	else if (ErrCode == 3)
	{
		//ErrCode = 3;
		voice = 36;
		t0=950;//only show "95 degree"
		LCM_Disp_Error(ErrCode, t0);
	}
	else
	{
		after_action=0;//20170320 jin
		ErrCode = 1;
		voice = 21;
		t0=675;//only show "67.5 degree"
		LCM_Disp_Error(ErrCode, t0);
	}

	Start_SleepCounter(59);		// 1 minutes, 60=1*60
	while (1)
	{
		Start_Hour_Count();
		if (Is_SleepCounter_CountDown_Finish())
		{
			Stop_SleepCounter();
			break;
		}
		if ((voice_count==0)||(((Soft_Timer[0] % 15)==0)&&(Soft_Timer[0]>20)))
		{
			SendMode(0, Operation_Mode);
			voice_count=1;	
			if(ErrCode==1)
				PlaySound(Language+voice);		// Voice
			else
				PlaySound(voice);		// Voice
			delay(20000);			
		}		
	}
	Operation_Mode = SLEEP_MODE;	
	ErrCode = 0;
}

void do_start_up(void)
{
	unsigned char key=0;
	INT=0;

	while (key != __KEY_STANDBY_MODE__)
	{
		key = ReceiveDate;
		SendMode(0, Operation_Mode);

		switch(key)
		{
				case __KEY_INFORMATION__:
					ReceiveDate = 0;
					PlaySound(33);
					while((key != __KEY_BACK__)&&(key != __KEY_DEMO_MODE__))
					{key = ReceiveDate;delay(1000);LCM_Disp_Version(VERSION_Y, VERSION_MD);SendVersion();}
					if(key == __KEY_DEMO_MODE__)
					{
						ReceiveDate = 0;
						demo_flag=1;
					}
					delay(1000);SendMode(3, 0);
					break;
		}
		delay(100);
		SendMode(2, 0);
	}
	
	if((f_clean) && (Faucent_Version_Flag))
	{
		Operation_Mode = CLEAN_MODE;
		start_up_mode=1;	
	}	
	else
	{
		Operation_Mode = STANDBY_MODE;
		start_up_mode=0;
	}
}
void main(void)
{

	PORT_Init();
	I2C_Init();
	LED_Init();
	LCM_Init();
	T0_Init();
	T1_Init();
	INT0_Init();	
	uart_init();
	EA = 1;
	LCM_Backlight(BL_Blue);
	LCM_Fill();
	SetCurrent(0);
	Buzzer(0);
	f_key_hold = 0;
	f_no_water = 0;
	SleepEnable = 1;
	Key_backup = 0;
	Soft_Timer_Enable = 0;
	LDS6128_init();
	EX0 = 0;
	SET_X1();
	SET_X2();
	SET_X3();
	//Set INT# to input pin and recievied low when connect with SAM3S   
	P3M1 = 0xF2;//Set UART port(P3.1),INT#(P3.3) to input
 	P3M0 = 0x0D;	
	KX &= ~0x1F0;
	if(INT)//0: VS-705, 1: PL-705
		delay(3000);//add delay for Holtec issue
	if((EEPROM_read_byte(20)>5)||((Choose_Default)&&(EEPROM_read_byte(38)>6))||((EEPROM_read_byte(20))&&(EEPROM_read_byte(38)<=6)&&((EEPROM_read_byte(38)!=1)))||((!EEPROM_read_byte(20))&&((EEPROM_read_byte(38)>6)||((EEPROM_read_byte(38)==1)))))
	{
		EEPROM_write_byte(20, DEFAULT_LANGUAGE);
		EEPROM_write_byte(38, ~Choose_Default);
	}
	LCM_Disp_Version(VERSION_Y, VERSION_MD);
	LCM_ClrL();
	//PlaySound(34);   // 34 開機聲
//if ((EEPROM_read_byte(0) != 'A')||(EEPROM_read_byte(1) != '7')||(EEPROM_read_byte(2) != (VERSION_Y))||(EEPROM_read_byte(3) != (VERSION_MD%256))||(EEPROM_read_byte(4) != (VERSION_MD/256)))
	if ((EEPROM_read_byte(2) != (VERSION_Y))||(EEPROM_read_byte(3) != (VERSION_MD%256))||(EEPROM_read_byte(4) != (VERSION_MD/256)))//20190213 jin
	{
		EEPROM_write_byte(0, 'A');	
		EEPROM_write_byte(1, '7');
		EEPROM_write_byte(2, VERSION_Y);
		EEPROM_write_byte(3, VERSION_MD%256);	
		EEPROM_write_byte(4, VERSION_MD/256);	
		EEPROM_WriteDefault();
	
	}
	if ((EEPROM_read_byte(0) != 'A')||(EEPROM_read_byte(1) != '7')) //20190130 jin new version don't set default.
	{
		EEPROM_write_byte(0, 'A');	
		EEPROM_write_byte(1, '7');
		EEPROM_write_byte(2, VERSION_Y);
		EEPROM_write_byte(3, VERSION_MD%256);	
		EEPROM_write_byte(4, VERSION_MD/256);	
		EEPROM_WriteDefault();
		EEPROM_WriteHideData();
		left_FlowCnt = 0;		
		Liter = 0;			
		UseMin = 0;			
		UseHour = 0;						
		EEPROM_write_word(21, left_FlowCnt);	// left_FlowCnt on EEPROM address 21~22
		delay(300);
		EEPROM_write_word(23, Liter);	// Liter on EEPROM address 23~24
		delay(300);
		EEPROM_write_word(25, UseMin);	// filter used sec 25~26
		delay(300);
		EEPROM_write_word(27, UseHour);	// filter used hour 27~28
	}	
	sendchar(EEPROM_read_byte(38));//sent area
	//if(EEPROM_read_byte(20)==0)sendchar(EEPROM_read_byte(38));//send TAIWAN area when language is Chinese
	//else sendchar(Area_Eng);//Send ENG language type
	ErrCode = 0;//Initail ErrCode
	WashLiter = tab_wash[EEPROM_read_byte(19)];	// WaterLiter papameter on EEPROM address 19 (0~5)
	if(WashLiter > 60)
	{	
		WashLiter = DEFAULT_WASHLITER;
		EEPROM_write_word(19, ((DEFAULT_WASHLITER/10)-1));
	}	
	if(EEPROM_read_byte(38)>6){EEPROM_write_byte(20, LAN_ENGLISH);}
	Language = (EEPROM_read_byte(20)*32);		// Language papameter on EEPROM address 20
	left_FlowCnt = EEPROM_read_word(21);		// left_FlowCnt on EEPROM address 21~22
	Liter = EEPROM_read_word(23);				// Liter on EEPROM address 23~24 (0~9000)
	sendchar(WashLiter);
	UseMin = EEPROM_read_word(25);			// filter used min 25~26 (0~3600 i.e. 1 hour)
	UseHour = EEPROM_read_word(27);			// filter used hour 27~28 (0~365*24)
	sendchar(__KEY_A705R04__);
	
	
	Usage_Counter[0] = EEPROM_read_word(39) + (EEPROM_read_word(41)<<16);//number of usage on Alka 4 (EEPROM 39~42)
	Usage_Counter[1] = EEPROM_read_word(43) + (EEPROM_read_word(45)<<16);//number of usage on Alka 3 (EEPROM 43~46)
	Usage_Counter[2] = EEPROM_read_word(47) + (EEPROM_read_word(49)<<16);//number of usage on Alka 2 (EEPROM 47~50)
	Usage_Counter[3] = EEPROM_read_word(51) + (EEPROM_read_word(53)<<16);//number of usage on Alka 1 (EEPROM 51~54)
	Usage_Counter[4] = EEPROM_read_word(55) + (EEPROM_read_word(57)<<16);//number of usage on FILTER (EEPROM 55~58)
	Usage_Counter[5] = EEPROM_read_word(59) + (EEPROM_read_word(61)<<16);//number of usage on Acid 1 (EEPROM 59~62)
	Usage_Counter[6] = EEPROM_read_word(63) + (EEPROM_read_word(65)<<16);//number of usage on Acid 2 (EEPROM 63~66)
	Usage_Counter[7] = EEPROM_read_word(67) + (EEPROM_read_word(69)<<16);//number of usage on Acid 3 (EEPROM 67~70)
	electrolyze_time = EEPROM_read_word(71) + (EEPROM_read_word(73)<<16);//time of electrolysis (EEPROM 71~74)
	Outwater_Counter = EEPROM_read_word(75) + (EEPROM_read_word(77)<<16);//time of outwater(EEPROM 75~78)
	overflow_flag = EEPROM_read_word(79);//overflow flag on address of EEPROM 79~80
	if((overflow_flag|0xAFFF)!=0xAFFF)
		overflow_flag=0;
	

	//UseHour = 8759;
	//Liter = 5000;
	//UseMin = 0;
	LeftLiter = MAX_LITER-Liter;


	
	//Volume = EEPROM_read_byte(18);
	Volume = f_lock;
	if (Volume & 0x80)		// LOCK papameter on EEPROM address 18, 7:f_lock 5..0:electrolysis_type
	{
		electrolysis_type = Volume & 0x7F;
		f_lock = 1;
	}
	else
	{
		electrolysis_type = WALKA3;
		f_lock = 0;
	}
	Volume = 2;
	PlaySound(0x30+Volume);
	Disp_test();
	LCM_Cls();

	ADS1110_flag=ADS1110_read_voltage(2);
	LCM_Disp_VOL_K2K3();
	Filter_time_is_up();
	LCM_Disp_KL(MAX_LITER-Liter, 0);
	I_add_t = EEPROM_read_byte(30);
	I_add[0] = EEPROM_read_byte(31);
	I_add[1] = EEPROM_read_byte(32);
	I_add[2] = EEPROM_read_byte(33);
	I_add[3] = EEPROM_read_byte(34);
	I_add[4] = 0;
	I_add[5] = EEPROM_read_byte(35);
	I_add[6] = EEPROM_read_byte(36);
	I_add[7] = EEPROM_read_byte(37);
	Init_ADC();
	Set_U7_10k();
	//Recieved from INT of SAM3S 
	Control_Selection = INT;
	//Change INT pin from input to Pull Up output and send outwater message to SAM3S 
	PORT_Init();
	
	if (UseHour >= MAX_HOUR || LeftLiter<=0) //20190403 jin
		EEPROM_write_byte(81, 1); //don't clean
		
	
	if(EEPROM_read_byte(81))
		f_clean=0;
	else
		f_clean=1;
	
	if(Control_Selection == 0)
	{
			sendchar(0xB0+EEPROM_read_byte(38));
		
			Operation_Mode = START_UP_MODE;
			start_up_mode=1;
	}
	else if(f_clean)
			Operation_Mode = CLEAN_MODE;
	else
			Operation_Mode = STANDBY_MODE;	
	while (1)
	{

		switch (Operation_Mode)
		{
			case START_UP_MODE:
				SendMode(0, Operation_Mode);
				do_start_up();
				break;
			
			case STANDBY_MODE:
				SendMode(0, Operation_Mode);
				do_standby();
				break;

			case ELECTROLYSIS_MODE:
				SendMode(0, Operation_Mode);
				do_electrolyze();
				break;

			case SLEEP_MODE:
				SendMode(0, Operation_Mode);
				do_sleep();
				break;

			case CLEAN_MODE:
				SendMode(0, Operation_Mode);
			   if(clean_after_trun_off== 1)
				 {do_clean(0);}
				else if(f_clean == 1)
				 { do_clean(1);}
	            else if ((f_acid3 == 1)&&(f_clean == 0))
				 { do_clean(0);}
				 else
				 { do_clean(0);}
			
				break;
			
			case WARNING_MODE:
				SendMode(0, Operation_Mode);
				do_warning();
				break;

			case RESET_MODE:
				do_reset();
				break;

			case SETUP_MODE:
				do_setup();
				break;
		}
	}
}