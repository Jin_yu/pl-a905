
#include <COUNTER.H>

extern unsigned char Soft_Timer_Enable,Operation_Mode,electrolysis_type;
extern unsigned int overflow_flag,Over_each_flag[10];
extern bit f_clean;
extern unsigned int UseHour, UseMin, max_hour_count;
extern unsigned long int electrolyze_time;
extern idata unsigned char Soft_Timer[8];


/*
================================================================
  Sleep Counter function  
  Soft_Timer[0] for sleep counter, unit:sec
================================================================
*/

void Start_SleepCounter(unsigned char sec)
{
	Soft_Timer[0] = sec;				// sec seconds	
	Soft_Timer_Enable |= 0x01;			// enable Soft_Timer[0]
}

void Stop_SleepCounter(void)		
{
	Soft_Timer_Enable &= ~0x01;
	Soft_Timer[0] = 0;
}

unsigned char Is_SleepCounter_CountDown_Finish(void)
{
	unsigned c;
	
	c = (Soft_Timer[0]==0)? 1 : 0;

	return c;
}

unsigned char Check_SleepCounter_Start(void)		// if sleep counter is started return 1
{
	unsigned c;

	c = (Soft_Timer_Enable & 0x01)? 1 : 0;

	return c;
}
//==============================================================

/*
================================================================
  Keyhold Counter function  
  Soft_Timer[1] for key hold counter, unit:sec
================================================================
*/

void Start_KeyholdCounter(unsigned char sec)
{
	Soft_Timer[1] = sec;		// sec seconds	
	Soft_Timer_Enable |= 0x02;	// enable Soft_Timer[1]
} 

void Stop_KeyholdCounter(void)
{
	Soft_Timer_Enable &= ~0x02;
	Soft_Timer[1] = 0;
} 
/*
unsigned char Check_KeyholdCounter_Start(void)		// if key hold counter is started return 1
{
	unsigned c;

	c = (Soft_Timer_Enable & 0x02)? 1 : 0;

	return c;
}
*/
unsigned char Is_KeyholdCounter_CountDown_Finish(void)
{
	unsigned c;
	
	c = (Soft_Timer[1]==0)? 1 : 0;

	return c;
}
//==============================================================
/*
================================================================
  VoiceWait Counter function  
  Soft_Timer[2] for water lack counter, unit:sec
================================================================
**/
void Start_Hour_Count(void)
{
	if(Soft_Timer[2]==0 && UseHour < max_hour_count)
	{		
			UseMin++;
			EEPROM_write_word(25, UseMin);
			Soft_Timer[2] = 60;		// sec seconds	60 Sec=1min
			Soft_Timer_Enable |= 0x04;	// enable Soft_Timer[0]
			if(UseMin >= 60)// 60 min = 1hour
			{
					UseMin%=60;
					UseHour++;						
					EEPROM_write_word(27, UseHour);	
					if(electrolyze_time>=4294967295)
					{
						overflow_flag |=Over_each_flag[9];
						electrolyze_time=0;
					}
					if(((Operation_Mode==1)&&(electrolysis_type!=5))||((Operation_Mode==4)&&(f_clean==1)))	
						electrolyze_time++;
			}
	}
} 

//==============================================================


/*
================================================================
  WaterLack Counter function  
  Soft_Timer[3] for water lack counter, unit:sec
================================================================
*/

void Start_WaterLackCounter(unsigned char sec)
{
	Soft_Timer[3] = sec;		// sec seconds	
	Soft_Timer_Enable |= 0x08;	// enable Soft_Timer[0]
} 

void Stop_WaterLackCounter(void)
{
	Soft_Timer_Enable &= ~0x08;
	Soft_Timer[3] = 0;
} 

unsigned char Check_WaterLackCounter_Start(void)		// if water lack counter is started return 1
{
	unsigned c;

	c = (Soft_Timer_Enable & 0x08)? 1 : 0;

	return c;
}

unsigned char Is_WaterLackCounter_CountDown_Finish(void)
{
	unsigned c;
	
	c = (Soft_Timer[3]==0)? 1 : 0;

	return c;
}
//==============================================================


