/*
*********************************************************************************************************
* Copyright 2008 Leadis Technology, Inc. ALL RIGHTS RESERVED.
* This software is provided under license and contains proprietary and
* confidential material which is the property of IDT Technology, Inc.
*
*
* FileName     : i2c.h
* Description  : Default Atmel AT89S52 GPIO to ctrl i2c function.
*
*
* Version control:
*  $Revision: 0.1 $    Date: 2008/10/10 12:00:00   ue_fu@leadis.com
*      first implemetation
*
*********************************************************************************************************
*/

#ifndef __I2C_H__
#define __I2C_H__


#define __KEY_NON__									0
#define __KEY_ACTION__							    1
#define __KEY_ALKA4__								2
#define __KEY_ALKA3__								3
#define __KEY_ALKA2__								4
#define __KEY_ALKA1__								5
#define __KEY_FILTER__							    6
#define __KEY_ACID1__								7
#define __KEY_ACID2__								8
#define __KEY_ACID3__								9
#define __KEY_MENU__					 			10
#define __KEY_LOCK__					 			11
#define __KEY_ENTER__					 			12
#define __KEY_VOL_ADJ__				 			    13
#define __KEY_CLEAR__					 			14
#define __KEY_PS_KEY__				 			    15
#define __KEY_RESET__					 			16
#define __KEY_VOL_3__					 			17
#define __KEY_VOL_2__					 			18
#define __KEY_VOL_1__					 			19
#define __KEY_VOL_0__					 			20
#define __KEY_LAG_C__					 			21
#define __KEY_LAG_C_N__				 			    22
#define __KEY_LAG_C_M__				 			    23
#define __KEY_LAG_C_S__				 			    24
#define __KEY_LAG_C_E__				 			    25
#define __KEY_LAG_E__					 			26
#define __KEY_SET_ELE_ALK4__	 			        27
#define __KEY_SET_ELE_ALK3__	 			        28
#define __KEY_SET_ELE_ALK2__	 			        29
#define __KEY_SET_ELE_ALK1__	 			        30
#define __KEY_SET_ELE_ACI1__	 			        31
#define __KEY_SET_ELE_ACI2__	 			        32
#define __KEY_SET_ELE_ACI3__	 			33
#define __KEY_SET_ELE__				 			    34
#define __KEY_BACK__					 			35
#define __KEY_SET_ELE_RST__		 		        	36		
#define __KEY_SET_CLEAN_10L__	 			        37
#define __KEY_SET_CLEAN_20L__	 			        38
#define __KEY_SET_CLEAN_30L__	 			        39
#define __KEY_SET_CLEAN_40L__	 			        40
#define __KEY_SET_CLEAN_50L__	 			        41
#define __KEY_SET_CLEAN_60L__	 			        42
#define __KEY_INFORMATION__		 			        43
#define __KEY_DIN_SOUND__			 			    44
#define __KEY_PENDING_STATUS__ 			            45	
#define __KEY_HIDDEN_PAGE__		 			        46
#define __KEY_DEMO_MODE__			 			    47
#define __KEY_STANDBY_MODE__	 			        48
#define __KEY_USAGE_COUNT_ALK4__	 	49
#define __KEY_USAGE_COUNT_ALK3__	 	50
#define __KEY_USAGE_COUNT_ALK2__	 	51
#define __KEY_USAGE_COUNT_ALK1__	 	52
#define __KEY_USAGE_COUNT_FILTER__	53
#define __KEY_USAGE_COUNT_ACI1__	 	54
#define __KEY_USAGE_COUNT_ACI2__	 	55
#define __KEY_USAGE_COUNT_ACI3__	 	56
#define __KEY_ELECTROLYZE_TIME__	 	57
#define __KEY_OUTWATER_COUNT__	 		58
#define __KEY_LAN_LOOP__	 					59
#define __KEY_START_CLEANING__	 		60
#define __KEY_DIN_SOUND_2__					61
#define __KEY_A705R04__							62
#define __KEY_CLEAN_WITH_PJ6000__		63
#define __KEY_FAUCET_SLEEP__				64

void    I2C_Init(void);
void 	EEPROM_write_byte(unsigned char reg_addr, unsigned char reg_data);
unsigned char	EEPROM_read_byte(unsigned char reg_addr);
void DAC_write(unsigned char ch_id, unsigned int ma);
void LDS6128_init(void);
unsigned char KeyScan(void);
void PlaySound(unsigned char num);
unsigned char voicePlay(unsigned char id, unsigned char vol);
unsigned char Get_Key(void);
void Clear_key_delay(void);
unsigned int ADS1110_read_voltage(unsigned char status);//0:output register 1: output current 2: ack flag
unsigned char TPL0401A_Set_R(unsigned char rdata);
unsigned int ADS1115_Read(unsigned char port,unsigned char status);
unsigned char TPL0401A_Read_R(void);
#define         TRUE	1
#define         FALSE	0

#endif  /* __I2C_H__ */

